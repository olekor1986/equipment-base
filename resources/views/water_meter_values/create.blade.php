@extends('templates.bigpicture')


@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-7">Добавление показаний водомеров</h2>
            <a href="/" class="btn btn-sm btn-success" role="button">На главную</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="card border-primary mb-3">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>Адрес</th>
                        <th>Описание</th>
                        <th>Предыдущие показания</th>
                        <th>Настоящие показания</th>
                        <th></th>
                    </tr>
                        @foreach($sources as $source)
                            @if(count($source->water_meters))
                                @foreach($source->water_meters as $water_meter)
                                @if($water_meter->condition != 'в работе')
                                    @continue
                                @endif
                                <tr>
                                    <td>{{$water_meter->source->address}}</td>
                                    <td>
                                        <a href="/water_meters/{{$water_meter->id}}">
                                            {{$water_meter->title .  ', ' . $water_meter->number
                                                                                .  ', ' . 'DN' . $water_meter->diameter
                                                                                .  ', ' . $water_meter->purpose}}
                                        </a>
                                    <td>
                                    @foreach($water_meter->water_meter_values as $item)
                                           @if($item->month == $datePrev->isoFormat('YYYY-MM'))
                                                @if ($item->created_at == NULL)
                                                    {{$item->value}}
                                                    @break
                                                @endif
                                                @if ($item->created_at != NULL)
                                                    {{$item->value}}
                                                    @break
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @php
                                            $addValue = false;
                                        @endphp
                                        @foreach($water_meter->water_meter_values as $item)
                                            @if($item->month == $dateNow->isoFormat('YYYY-MM'))
                                            @php
                                                $addValue = true;
                                            @endphp
                                                <span style="color: green">
                                                    @if ($item->created_at == NULL)
                                                        {{$item->value}}
                                                        @break
                                                    @endif
                                                    @if ($item->created_at != NULL)
                                                        {{$item->value}}
                                                        @break
                                                    @endif
                                                </span>
                                            @else
                                                <span style="color: red">{{'показания отсутсвуют'}}</span>
                                                @break
                                            @endif
                                        @endforeach
                                    </td>
                                    @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                                        <td>
                                            @if($addValue == false)
                                            <form class ="form-horizontal" action="/water_meter_values" method="post">
                                                @csrf
                                                <input type="hidden" name="water_meter_id" value="{{$water_meter->id}}">
                                                <input type="hidden" name="month" value="{{$dateNow->locale('ru')->isoFormat('YYYY-MM')}}">
                                                <div class="form-group">
                                                    <label>Внести показания за {{$dateNow->locale('ru')->isoFormat('MMMM YYYY')}}:
                                                        <input type="text" class="form-control" name="value">
                                                    </label>
                                                    <label>Примечание:
                                                        <input type="text" class="form-control" name="note">
                                                    </label>
                                                </div>
                                                @if(Auth::user()->user_info->user_role == 'admin')
                                                <div class="form-group">
                                                    <label>
                                                        <input type="checkbox" name="after_check">
                                                        показания после поверки</label>
                                                </div>
                                                @endif
                                                <button class="btn btn-sm btn-success">Сохранить</button>
                                            </form>
                                            @endif
                                            <a role="button" class="btn btn-sm btn-warning" data-toggle="collapse" href="#collapseExample{{$water_meter->id}}"
                                               aria-expanded="false" aria-controls="collapseExample{{$water_meter->id}}">
                                                история
                                            </a>
                                            <div class="collapse" id="collapseExample{{$water_meter->id}}">
                                                <div class="card card-border">
                                                    <table class="table table-sm">
                                                        <tr>
                                                            <th>Месяц</th>
                                                            <th>Показания, м3</th>
                                                            <th></th>
                                                        </tr>
                                                        @php
                                                            $i = 1;
                                                        @endphp
                                                        @foreach($water_meter->water_meter_values as $item)
                                                            @if($i == 5)
                                                                @break
                                                            @endif
                                                            <tr>
                                                                <td>{{$item->month}}</td>
                                                                <td>{{$item->value}}</td>
                                                                <td>
                                                                    <a href="/water_meter_values/{{$item->id}}/edit" class="btn btn-sm btn-warning" role="button">Изменить</a>
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $i++;
                                                            @endphp
                                                        @endforeach
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection



