@extends('templates.bigpicture')
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/water_meter_values/{{$water_meter_value->id}}" method="post">
                            @csrf
                            @method('put')
                            <input type="hidden" value="{{$water_meter_value->id}}" name="id">
                            <input type="hidden" value="{{$water_meter_value->water_meter_id}}" name="water_meter_id">
                            <div class="form-group">
                                <label>Значение:
                                    <input type="text" class="form-control" name="value" value="{{$water_meter_value->value}}">
                                </label>
                                <label>Примечание:
                                    <input type="text" class="form-control" name="note" value="{{$water_meter_value->note}}">
                                </label>
                            </div>
                            @if(Auth::user()->user_info->user_role == 'admin')
                            <div class="form-group">
                                <label>
                                    <input type="checkbox" name="after_check"
                                    @if($water_meter_value->after_check == true)
                                        {{ 'checked' }}
                                    @endif
                                    >
                                    показания после поверки</label>
                            </div>
                            @endif
                                <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
