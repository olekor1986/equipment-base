@extends('templates.bigpicture')


@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-7">TEST</h2>
            <a href="/" class="btn btn-sm btn-success" role="button">На главную</a>
        </div>
    </div>
@endsection
@section('content')
<div class="container">
   <ol>
       @foreach($sources as $source)
           @if(count($source->water_meters))
                <li>
                    {{$source->address}}
                        <ul>
                            @foreach($source->water_meters as $meter)
                                <li>
                                    {{$meter->title}}
                                        <ul>
                                            @foreach($meter->water_meter_values as $value)
                                                <li>
                                                    {{$value->month . '---' . $value->value}}
                                                </li>
                                            @endforeach
                                        </ul>
                                </li>
                            @endforeach
                        </ul>
                </li>
           @endif
        @endforeach
   </ol>
</div>
@endsection
