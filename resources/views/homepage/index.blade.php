@extends('templates.bigpicture')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                @if(Auth::check())
                    <h2 class="mt-5">Добро пожаловать {{Auth::user()->user_info->first_name}}!</h2>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('content')
    @if(isset(Auth::user()->user_info->user_role))
        @if(Auth::user()->user_info->user_role == 'user')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        @if(Auth::user()->user_info->user_role == 'admin')
                            <p>
                                <a id="homepage-button" href="/water_meter_report" role="button" class="btn btn-lg btn-primary">
                                    Отчет по воде за месяц
                                </a>
                            </p>
                        @endif
                        <p>
                            <a id="homepage-button" href="/water_meter_values/create" role="button" class="btn btn-lg btn-primary">
                                Внести показания водомеров
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <a id="homepage-button" href="/monitoring" role="button" class="btn btn-lg btn-warning">
                                Мониторинг параметров котельных
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/sources" role="button" class="btn btn-lg btn-success">
                                Источники тепла
                            </a>
                        </p>
                        <!--
                        <p>
                            <a id="homepage-button" href="/interactive_map" role="button" class="btn btn-lg btn-success">
                                Источники тепла на карте
                            </a>
                        </p>
                        -->
                        <p>
                            <a id="homepage-button" href="/boilers" role="button" class="btn btn-lg btn-success">
                                Котлы
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/pumps" role="button" class="btn btn-lg btn-success">
                                Насосы
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/water_meters" role="button" class="btn btn-lg btn-success">
                                Водомеры
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/heating_pipelines" role="button" class="btn btn-lg btn-success">
                                Теплотрассы
                            </a>
                        </p>
                    </div>
                    </div>
                </div>
            </div>
        @elseif(Auth::user()->user_info->user_role == 'guest')
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <p class="guest_message">
                            У Вас гостевая учетная запись, доступен только просмотр оборудования,
                            для изменения прав доступа, свяжитесь с администратором.
                        </p>
                    </div>
                </div>
            </div>
        @endif
    @endif
@endsection
