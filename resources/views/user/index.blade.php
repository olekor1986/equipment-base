@extends('templates.bigpicture')

@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">Профиль пользователя {{$user->name}}</h2>
        </div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
            @if(isset($user->user_info->user_id))
                <ul>
                    <li>Фамилия: {{$user->user_info->last_name}}</li>
                    <li>Имя: {{$user->user_info->first_name}}</li>
                    <li>Отчество: {{$user->user_info->middle_name}}</li>
                    <li>Рабочий телефон: {{$user->user_info->w_phone}}</li>
                    <li>Мобильный телефон: {{$user->user_info->m_phone}}</li>
                    <li>E-mail: {{$user->email}}</li>

                </ul>
                <a href="/user/{{$user->id}}/edit" class="btn btn-lg btn-warning">Редактировать данные</a>
                <a href="/password/reset" class="btn btn-lg btn-danger">Сбросить пароль</a>
            @else
                <form class="form-horizontal" action="/user_info" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Фамилия
                            <input name="last_name" type="text">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Имя
                            <input name="first_name" type="text">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Отчество
                            <input name="middle_name" type="text">
                        </label>
                    </div>
                    <div class="form-group">
                        <label>Рабочий телефон
                            <input name="w_phone" type="text">
                        </label>
                        <label>Мобильный телефон
                            <input name="m_phone" type="text">
                        </label>
                    </div>
                    <button class="btn btn-lg btn-success">Сохранить</button>
                </form>
            @endif
        </div>
    </div>
</div>
@endsection
