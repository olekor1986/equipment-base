@extends('templates.bigpicture')


@section('header')
<div class="header">
    <div class="starter-template">
        <h2>Мониторинг параметров котельных</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
<div class="container-fluid">
    <div class="card border-primary mb-3">
         <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Адрес</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($sources as $source)
                    @if ($source->type == 'building' ||
                    $source->type == 'ЦТП' ||
                    $source->type == 'ІТП')
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <a href="http://monitoring.novateplo.od.ua/viewHeatSourceData.php?sourceId={{$source->id}}" class="btn btn-sm btn-success" role="button">{{$source->address}}</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
