@extends('templates.bigpicture')
@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">{{'Характеристики участка теплосети котельной ' . $heatingPipeline->source->address}}</h2>
        <h2 class="display-7">{{$heatingPipeline->pipe_start . '  -  ' . $heatingPipeline->pipe_end}}</h2>
        <a href="/heating_pipelines" class="btn btn-sm btn-success" role="button"><- к списку</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                    <ul>
                        <li>От - {{$heatingPipeline->pipe_start}}</li>
                        <li>до - {{$heatingPipeline->pipe_end}}</li>
                        <li>Диаметр подачи -  {{$heatingPipeline->direct_diam}} мм</li>
                        <li>Диаметр обратки -  {{$heatingPipeline->reverse_diam}} мм</li>
                        <li>Длина участка - {{$heatingPipeline->length}} м</li>
                        <li>Тип -  {{$heatingPipeline->type}}</li>
                        <li>Способ прокладки - {{$heatingPipeline->method}}</li>
                        <li>Глубина/Высота прокладки -   {{$heatingPipeline->height}} м</li>
                        <li>Тип изоляции - {{$heatingPipeline->ins_type}}</li>
                        <li>Толщина изоляции - {{$heatingPipeline->ins_thick}}</li>
                        <li>Состояние изоляции - {{$heatingPipeline->ins_cond}}</li>
                        <li>Год прокладки - {{$heatingPipeline->build_year}}</li>
                        <li>Примечание - {{$heatingPipeline->note}}</li>
                    </ul>
                    </div>
                </div>
                @if($heatingPipeline->created_at)
                    <p>Создано: {{$heatingPipeline->updated_at . ' пользователем ' . $heatingPipeline->user->name}}</p>
                @endif
                @if($heatingPipeline->updated_at)
                    <p>Отредактировано: {{$heatingPipeline->updated_at . ' пользователем ' . $heatingPipeline->user->name}}</p>
                @endif
                @if(Auth::user()->user_info->user_role == 'admin')
                    <form action="/heating_pipelines/{{$heatingPipeline->id}}" method="post">
                        {{csrf_field()}}
                        @method('delete')
                        <button class="btn btn-sm btn-danger">Удалить</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
