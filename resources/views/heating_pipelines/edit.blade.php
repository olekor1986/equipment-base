@extends('templates.bigpicture')
@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">{{'Редактировать данные теплосети котельной ' . $heatingPipeline->source->address}}</h2>
        <h2 class="display-7">{{$heatingPipeline->pipe_start . '  -  ' . $heatingPipeline->pipe_end}}</h2>
        <a href="/heating_pipelines" class="btn btn-sm btn-success" role="button"><- назад к списку</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/heating_pipelines/{{$heatingPipeline->id}}" method="post">
                            {{csrf_field()}}
                            @method('put')
                            <div class="form-group">
                                <label>Источник тепла:
                                    <select name="source_id" class="form-control" required>
                                        <option value="{{$heatingPipeline->source_id}}" selected>{{$heatingPipeline->source->address}}</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}">{{$source->address}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <label>Тип:
                                    <select name="type" class="form-control" required>
                                        <option value="{{$heatingPipeline->type}}" selected>{{$heatingPipeline->type}}</option>
                                        <option value="розподільчий">розподільчий</option>
                                        <option value="магістральний">магістральний</option>
                                        <option value="гаряче водопостачання">гаряче водопостачання</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Начало участка:
                                    <input type="text" class="form-control" name="pipe_start" value="{{$heatingPipeline->pipe_start}}" required>
                                </label>
                                <label>Конец участка:
                                    <input type="text" class="form-control" name="pipe_end" value="{{$heatingPipeline->pipe_end}}" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Диаметр подачи, мм:
                                    <select name="direct_diam" class="form-control" required>
                                        <option value="{{$heatingPipeline->direct_diam}}" selected>{{$heatingPipeline->direct_diam}}</option>
                                        <option value="25">25</option>
                                        <option value="32">32</option>
                                        <option value="40">40</option>
                                        <option value="57">57</option>
                                        <option value="76">76</option>
                                        <option value="89">89</option>
                                        <option value="108">108</option>
                                        <option value="133">133</option>
                                        <option value="159">159</option>
                                        <option value="219">219</option>
                                        <option value="273">273</option>
                                        <option value="325">325</option>
                                    </select>
                                </label>
                                <label>Диаметр обратки, мм:
                                    <select name="reverse_diam" class="form-control" required>
                                        <option value="{{$heatingPipeline->reverse_diam}}" selected>{{$heatingPipeline->reverse_diam}}</option>
                                        <option value="25">25</option>
                                        <option value="32">32</option>
                                        <option value="40">40</option>
                                        <option value="57">57</option>
                                        <option value="76">76</option>
                                        <option value="89">89</option>
                                        <option value="108">108</option>
                                        <option value="133">133</option>
                                        <option value="159">159</option>
                                        <option value="219">219</option>
                                        <option value="273">273</option>
                                        <option value="325">325</option>
                                    </select>
                                </label>
                                <label>Длина участка, м:
                                    <input type="text" class="form-control" name="length" value="{{$heatingPipeline->length}}" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Способ прокладки:
                                    <select name="method" class="form-control" required>
                                        <option id="method_changed" value="{{$heatingPipeline->method}}" selected>{{$heatingPipeline->method}}</option>
                                        <option value="канальна">канальна</option>
                                        <option value="безканальна">безканальна</option>
                                        <option value="наземна">наземна</option>
                                    </select>
                                </label>
                                <label>Глубина/Высота прокладки, м:
                                    <input type="text" class="form-control" name="height" value="{{$heatingPipeline->height}}">
                                </label>
                                <label>Год прокладки:
                                    <input type="text" class="form-control" name="build_year" placeholder="если известен" value="{{$heatingPipeline->build_year}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Тип изоляции:
                                    <select name="ins_type" class="form-control" required>
                                        <option value="{{$heatingPipeline->ins_type}}" selected>{{$heatingPipeline->ins_type}}</option>
                                        <option value="мінвата">мінвата</option>
                                        <option value="попередньо-ізольований">попередньо-ізольований</option>
                                    </select>
                                </label>
                                <label>Толщина изоляции, мм:
                                    <input type="text" class="form-control" name="ins_thick" value="{{$heatingPipeline->ins_thick}}">
                                </label>
                                <label>Состояние изоляции:
                                    <select name="ins_cond" class="form-control" required>
                                        <option value="{{$heatingPipeline->ins_cond}}" selected>{{$heatingPipeline->ins_cond}}</option>
                                        <option value="вимагає заміни">вимагає заміни</option>
                                        <option value="задовільний">задовільний</option>
                                        <option value="відмінне">відмінне</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Примечание:
                                    <input type="text" class="form-control" name="note" value="{{$heatingPipeline->note}}">
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
