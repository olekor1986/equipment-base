@extends('templates.bigpicture')

@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">Теплотрассы</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
            <a href="/heating_pipelines/create" class="btn btn-sm btn-primary" role="button">Добавить новую</a>
        @endif
    </div>
</div>
@endsection
@section('content')
    <div class="container-fluid">
            <div class="list-group">
                @foreach($sources as $source)
                    @if ($source->type == 'building' || !count($source->heating_pipelines))
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <a id="heating-pipeline-button" class="list-group-item list-group-item-action" aria-current="true"
                       data-toggle="collapse" href="#collapseExample{{$source->id}}"
                       aria-expanded="false" aria-controls="collapseExample{{$source->id}}">
                        {{$source->address}}
                    </a>
                    <div class="collapse" id="collapseExample{{$source->id}}">
                        <div class="card border-primary">
                            <div class="table-responsive">
                                <table class="table table-sm table-striped">
                                    <tr>
                                        <th>От</th>
                                        <th>До</th>
                                        <th>Ф, подача</th>
                                        <th>Ф, обратка</th>
                                        <th>L, m</th>
                                        <th>Тип</th>
                                        <th>Прокладка</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    @foreach($source->heating_pipelines as $pipe)
                                        <tr>
                                            <td>{{$pipe->pipe_start}}</td>
                                            <td>{{$pipe->pipe_end}}</td>
                                            <td>{{$pipe->direct_diam}}</td>
                                            <td>{{$pipe->reverse_diam}}</td>
                                            <td>{{$pipe->length}}</td>
                                            <td>{{$pipe->type}}</td>
                                            <td>{{$pipe->method}}</td>
                                            <td>
                                                <a href="/heating_pipelines/{{$pipe->id}}" class="btn btn-sm btn-success" role="button">></a>
                                            </td>
                                            @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                                                <td>
                                                    <a href="/heating_pipelines/{{$pipe->id}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
        </div>
    </div>
@endsection
