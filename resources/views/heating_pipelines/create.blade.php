@extends('templates.bigpicture')

@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-7">Добавить новый участок теплосети</h2>
        <a href="/heating_pipelines" class="btn btn-sm btn-success" role="button"><- назад к списку</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/heating_pipelines" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Источник тепла:
                                    <select name="source_id" class="form-control" required>
                                        <option selected hidden disabled>Адрес...</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}">{{$source->address}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <label>Тип:
                                    <select name="type" class="form-control" required>
                                        <option selected hidden disabled>Выберите тип...</option>
                                        <option value="розподільчий">розподільчий</option>
                                        <option value="магістральний">магістральний</option>
                                        <option value="гаряче водопостачання">гаряче водопостачання</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Начало участка:
                                    <input type="text" class="form-control" name="pipe_start" required>
                                </label>
                                <label>Конец участка:
                                    <input type="text" class="form-control" name="pipe_end" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Диаметр подачи, мм:
                                    <select name="direct_diam" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="25">25</option>
                                        <option value="32">32</option>
                                        <option value="40">40</option>
                                        <option value="57">57</option>
                                        <option value="76">76</option>
                                        <option value="89">89</option>
                                        <option value="108">108</option>
                                        <option value="133">133</option>
                                        <option value="159">159</option>
                                        <option value="219">219</option>
                                        <option value="273">273</option>
                                        <option value="325">325</option>
                                    </select>
                                </label>
                                <label>Диаметр обратки, мм:
                                    <select name="reverse_diam" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="25">25</option>
                                        <option value="32">32</option>
                                        <option value="40">40</option>
                                        <option value="57">57</option>
                                        <option value="76">76</option>
                                        <option value="89">89</option>
                                        <option value="108">108</option>
                                        <option value="133">133</option>
                                        <option value="159">159</option>
                                        <option value="219">219</option>
                                        <option value="273">273</option>
                                        <option value="325">325</option>
                                    </select>
                                </label>
                                <label>Длина участка, м:
                                    <input type="text" class="form-control" name="length" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Способ прокладки:
                                    <select name="method" class="form-control" required>
                                        <option selected hidden disabled>Выберите тип...</option>
                                        <option value="канальна">канальна</option>
                                        <option value="безканальна">безканальна</option>
                                        <option value="наземна">наземна</option>
                                    </select>
                                </label>
                                <label>Глубина/Высота прокладки, м:
                                    <input type="text" class="form-control" name="height">
                                </label>
                                <label>Год прокладки:
                                    <input type="text" class="form-control" name="build_year" placeholder="если известен">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Тип изоляции:
                                    <select name="ins_type" class="form-control" required>
                                        <option selected hidden disabled>Выберите тип...</option>
                                        <option value="мінвата">мінвата</option>
                                        <option value="попередньо-ізольований">попередньо-ізольований</option>
                                    </select>
                                </label>
                                <label>Толщина изоляции, мм:
                                    <input type="text" class="form-control" name="ins_thick" value="0">
                                </label>
                                <label>Состояние изоляции:
                                    <select name="ins_cond" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="вимагає заміни">вимагає заміни</option>
                                        <option value="задовільний">задовільний</option>
                                        <option value="відмінне">відмінне</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Примечание:
                                    <input type="text" class="form-control" name="note">
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
