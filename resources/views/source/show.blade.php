@extends('templates.bigpicture')

@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-7">{{mb_strtoupper($source->type) . ' ' . $source->address}}</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <h4 class="card-header text-primary">Характеристики</h4>
                <ul>
                    <li>Район: {{$source->district}}</li>
                    <li>Тип: {{$source->type}}</li>
                    <li>Топливо: {{$source->fuel}}</li>
                    <li>Полная мощность:    </li>
                    <li>Присоединенная нагрузка: {{$source->connected_power}}</li>
                    <li>Мастер: {{$source->master->user_info->last_name . ' ' .
                        $source->master->user_info->first_name . ' ' .
                        $source->master->user_info->middle_name}}
                    </li>
                    <li>Старший мастер: {{$source->st_master->user_info->last_name . ' ' .
                        $source->st_master->user_info->first_name . ' ' .
                        $source->st_master->user_info->middle_name}}
                    </li>
                    <li>Электромонтер: {{$source->el_mounter->name}}</li>
                    <li>Мастер-электрик: {{$source->el_master->name}}</li>
                    <li>Слесарь КИП: {{$source->kip_mounter->name}}</li>
                    <li>Мастер КИП: {{$source->kip_master->name}}</li>
                </ul>
                <ul>
                    @if(Auth::user()->user_info->user_role == 'admin')
                        <form action="/sources/{{$source->slug}}" method="post">
                            {{csrf_field()}}
                            @method('delete')
                            <button class="btn btn-sm btn-danger">Удалить</button>
                        </form>
                    @endif
                    @if($source->created_at)
                        <p>Создано: {{$source->updated_at . ' пользователем ' . $source->user->name}}</p>
                    @endif
                    @if($source->updated_at)
                        <p>Отредактировано: {{$source->updated_at . ' пользователем ' . $source->user->name}}</p>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
@if(count($source->boilers))
    <div class="container">
        <div class="card border-primary mb-3">
            <h4 class="card-header text-primary">Котлы</h4>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">Марка</th>
                        <th scope="col">№</th>
                        <th scope="col">Теплоноситель</th>
                        <th scope="col">Мощность, Гкал/час</th>
                        <th scope="col">КПД, %</th>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <th></th>
                            <th></th>
                            <th></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($source->boilers as $boiler)
                        <tr>
                            <td>{{$boiler->title}}</td>
                            <td>{{$boiler->number}}</td>
                            <td>{{$boiler->energy_carrier}}</td>
                            <td>{{$boiler->power}}</td>
                            <td>{{$boiler->efficient}}</td>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/boilers/{{$boiler->id}}" class="btn btn-sm btn-success" role="button">></a>
                            </td>
                            <td>
                                <a href="/boilers/{{$boiler->id}}/edit" class="btn btn-sm btn-warning" role="button">Ред.</a>
                            </td>
                        @endif
                        @if(Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <form action="/boilers/{{$boiler->id}}" method="post">
                                    {{csrf_field()}}
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger">X</button>
                                </form>
                            </td>
                        @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <ul>
                    @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                        <a href="/boilers/create" class="btn btn-sm btn-primary" role="button">Добавить котел</a>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
@if(count($source->pumps))
    <div class="container">
        <div class="card border-primary mb-3">
            <h4 class="card-header text-primary">Насосы</h4>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>Марка</th>
                        <th>Q, м3/ч</th>
                        <th>Н, м</th>
                        <th>N, кВт</th>
                        <th>n, об/мин</th>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <th></th>
                            <th></th>
                            <th></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($source->pumps as $pump)
                        <tr>
                            <td>{{$pump->title}}</td>
                            <td>{{$pump->capacity}}</td>
                            <td>{{$pump->pressure}}</td>
                            <td>{{$pump->engine_power}}</td>
                            <td>{{$pump->engine_speed}}</td>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/pumps/{{$pump->id}}" class="btn btn-sm btn-success" role="button">-></a>
                            </td>
                            <td>
                                <a href="/pumps/{{$pump->id}}/edit" class="btn btn-sm btn-warning" role="button">Ред.</a>
                            </td>
                        @endif
                        @if(Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <form action="/pumps/{{$pump->id}}" method="post">
                                    {{csrf_field()}}
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger">X</button>
                                </form>
                            </td>
                        @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <ul>
                    @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                        <a href="/pumps/create" class="btn btn-sm btn-primary" role="button">Добавить насос</a>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
@if(count($source->water_meters))
    <div class="container">
        <div class="card border-primary mb-3">
            <h4 class="card-header text-primary">Водомеры</h4>
            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th>Адрес</th>
                        <th>Марка</th>
                        <th>Диаметр</th>
                        <th>Зав.номер</th>
                        <th>Назначение</th>
                        <th>Дата поверки</th>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <th></th>
                            <th></th>
                            <th></th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($source->water_meters as $water_meter)
                        <tr>
                            <td>{{$water_meter->source->address}}</td>
                            <td>{{$water_meter->title}}</td>
                            <td>{{$water_meter->diameter}}</td>
                            <td>{{$water_meter->number}}</td>
                            <td>{{$water_meter->purpose}}</td>
                            <td>{{$water_meter->check_date}}</td>
                            @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                                <td>
                                    <a href="/water_meters/{{$water_meter->id}}" class="btn btn-sm btn-success" role="button">></a>
                                </td>
                                <td>
                                    <a href="/water_meters/{{$water_meter->id}}/edit" class="btn btn-sm btn-warning" role="button">Ред.</a>
                                </td>
                            @endif
                            @if(Auth::user()->user_info->user_role == 'admin')
                                <td>
                                    <form action="/water_meters/{{$water_meter->id}}" method="post">
                                        {{csrf_field()}}
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">X</button>
                                    </form>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <ul>
                    @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                        <a href="/water_meters/create" class="btn btn-sm btn-primary" role="button">Добавить водомер</a>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
@if(count($source->heating_pipelines))
    <div class="container">
        <div class="card border-primary mb-3">
            <h4 class="card-header text-primary">Теплотрассы</h4>
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>От</th>
                        <th>До</th>
                        <th>Ф, подача</th>
                        <th>Ф, обратка</th>
                        <th>L, m</th>
                        <th>Тип</th>
                        <th>Прокладка</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($source->heating_pipelines as $pipe)
                        <tr>
                            <td>{{$pipe->pipe_start}}</td>
                            <td>{{$pipe->pipe_end}}</td>
                            <td>{{$pipe->direct_diam}}</td>
                            <td>{{$pipe->reverse_diam}}</td>
                            <td>{{$pipe->length}}</td>
                            <td>{{$pipe->type}}</td>
                            <td>{{$pipe->method}}</td>
                            <td>
                                <a href="/heating_pipelines/{{$pipe->id}}" class="btn btn-sm btn-success" role="button">></a>
                            </td>
                            @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                                <td>
                                    <a href="/heating_pipelines/{{$pipe->id}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </table>
                <ul>
                    @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                        <a href="/heating_pipelines/create" class="btn btn-sm btn-primary" role="button">Добавить участок теплотрассы</a>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card border-primary mb-3">
                <h4 class="card-header text-primary">Местонахождение на карте г. Одесса</h4>
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var map;
    DG.then(function () {
        map = DG.map('map', {
            center: [{{$source->gps}}],
            zoom: 22,
            zoomSnap: 0.2,
            dragging: false,
            scrollWheelZoom: false
        });
        DG.marker([{{$source->gps}}]).addTo(map).bindLabel('{{$source->address}}');
    });
</script>
@endsection
