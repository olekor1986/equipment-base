@extends('templates.bigpicture')


@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-7">{{('Добавить новый источник тепла')}}</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/sources" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Адрес:
                                    <input type="text" class="form-control" name="address">
                                </label>
                                <label>Район города
                                    <select name="district" class="form-control">
                                        <option selected hidden disabled>Район города...</option>
                                        <option value="Приморский">Приморский</option>
                                        <option value="Малиновский">Малиновский</option>
                                        <option value="Суворовский">Суворовский</option>
                                        <option value="Киевский">Киевский</option>
                                    </select>
                                </label>
                                <label>Тип
                                    <select name="type" class="form-control">
                                        <option selected hidden disabled>Выберите тип...</option>
                                        <option value="котельня">Котельная</option>
                                        <option value="ЦТП">ЦТП</option>
                                        <option value="IТП">ИТП</option>
                                        <option value="адмiнбудiвля">адмiнбудiвля</option>
                                    </select>
                                </label>
                                <label>Баланс
                                    <select name="balance" class="form-control">
                                        <option value="баланс" selected hidden disabled>баланс</option>
                                        <option value="баланс">баланс</option>
                                        <option value="обслуживание котельной">обслуживание котельной</option>
                                        <option value="покупное тепло">покупное тепло</option>
                                        <option value="теплосети на балансе">теплосети на балансе</option>
                                    </select>
                                </label>
                                <label>Топливо
                                    <select name="fuel" class="form-control">
                                        <option selected hidden disabled>Выберите топливо...</option>
                                        <option value="газ">Газ</option>
                                        <option value="вугілля">Уголь</option>
                                        <option value="0">Нет</option>
                                    </select>
                                </label>
                                @if(Auth::user()->user_info->user_role == 'admin')
                                <label>Мониторинг
                                    <select name="monitoring" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="1">1</option>
                                        <option value="0">0</option>
                                    </select>
                                </label>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Полная мощность
                                    <input type="text" class="form-control" name="full_power" value="0">
                                </label>
                                <label>Присоединенная нагрузка
                                    <input type="text" class="form-control" name="connected_power" value="0">
                                </label>
                                <label>В работе
                                    <select name="in_work" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="1">1</option>
                                        <option value="0">0</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Старший мастер:
                                    <select name="st_master_id" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($employees['st_masters'] as $st_master)
                                            @if($st_master->user_info != NULL)
                                            <option value="{{$st_master->id}}">
                                                {{$st_master->user_info->last_name . ' '}}
                                                {{$st_master->user_info->first_name . ' '}}
                                                {{$st_master->user_info->middle_name}}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                                <label>Мастер:
                                    <select name="master_id" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($employees['masters'] as $master)
                                            @if($master->user_info != NULL)
                                            <option value="{{$master->id}}">
                                                {{$master->user_info->last_name . ' '}}
                                                {{$master->user_info->first_name . ' '}}
                                                {{$master->user_info->middle_name}}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Мастер-электрик:
                                    <select name="el_master_id" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($employees['el_masters'] as $el_master)
                                            <option value="{{$el_master->id}}">{{$el_master->name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <label>Электромонтер:
                                    <select name="el_mounter_id" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($employees['el_mounters'] as $el_mounter)
                                            <option value="{{$el_mounter->id}}">{{$el_mounter->name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Мастер КИПиА:
                                    <select name="kip_master_id" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($employees['kip_masters'] as $kip_master)
                                            <option value="{{$kip_master->id}}">{{$kip_master->name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <label>Слесарь КИПиА:
                                    <select name="kip_mounter_id" class="form-control">
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($employees['kip_mounters'] as $kip_mounter)
                                            <option value="{{$kip_mounter->id}}">{{$kip_mounter->name}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>GPS координаты
                                    <input type="text" class="form-control" name="gps" value="46.464616, 30.736500">
                                </label>
                           </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
