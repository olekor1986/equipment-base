@extends('templates.bigpicture')


@section('header')
<div class="header">
    <div class="starter-template">
        <h2>Источники тепла</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
            <a href="/sources/create" class="btn btn-sm btn-primary" role="button">Добавить новый</a>
        @endif
    </div>
</div>
@endsection
@section('content')
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">В эксплуатации</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Тип</th>
                    <th scope="col">Топливо</th>
                    <th scope="col">Мощность</th>
                    <th scope="col">Прис.нагр.</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($sources['in_work'] as $source)
                    @if ($source->type == 'building' ||
                    $source->type == 'ЦТП' ||
                    $source->type == 'ІТП')
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$source->address}}</td>
                        <td>{{$source->type}}</td>
                        <td>{{$source->fuel}}</td>
                        <td>{{$source->full_power}}</td>
                        <td>{{$source->connected_power}}</td>
                        <td>
                            <a href="/sources/{{$source->slug}}" class="btn btn-sm btn-success" role="button">></a>
                        </td>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                        <td>
                            <a href="/sources/{{$source->slug}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                        </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">ЦТП, ИТП</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Тип</th>
                    <th scope="col">Мощность</th>
                    <th scope="col">Прис.нагр.</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($sources['in_work'] as $source)
                    @if ($source->type == 'building' ||
                    $source->type == 'котельня')
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$source->address}}</td>
                        <td>{{$source->type}}</td>
                        <td>{{$source->full_power}}</td>
                        <td>{{$source->connected_power}}</td>
                        <td>
                            <a href="/sources/{{$source->slug}}" class="btn btn-sm btn-success" role="button">></a>
                        </td>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/sources/{{$source->slug}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">Админздания</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Тип</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($sources['buildings'] as $source)
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$source->address}}</td>
                        <td>{{$source->type}}</td>
                        <td>
                            <a href="/sources/{{$source->slug}}" class="btn btn-sm btn-success" role="button">></a>
                        </td>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/sources/{{$source->slug}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">Выведенные из эксплуатации</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th scope="col">№</th>
                    <th scope="col">Адрес</th>
                    <th scope="col">Тип</th>
                    <th scope="col">Топливо</th>
                    <th scope="col">Мощность</th>
                    <th scope="col">Прис.нагр.</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                @php
                    $i = 1;
                @endphp
                @foreach($sources['not_work'] as $source)
                    @if ($source->type == 'building')
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$source->address}}</td>
                        <td>{{$source->type}}</td>
                        <td>{{$source->fuel}}</td>
                        <td>{{$source->full_power}}</td>
                        <td>{{$source->connected_power}}</td>
                        <td>
                            <a href="/sources/{{$source->slug}}" class="btn btn-sm btn-success" role="button">></a>
                        </td>
                        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/sources/{{$source->slug}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
