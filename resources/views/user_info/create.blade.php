@extends('templates.empty')

@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">Добавить данные нового пользователя {{Auth::user()->name}}</h2>
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal" action="/user_info" method="post">
                            @csrf
                            <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
                            <div class="form-group row">
                                <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Фамилия') }}</label>
                                <div class="col-md-6">
                                    <input id="last_name" class="form-control" name="last_name" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('Имя') }}</label>
                                <div class="col-md-6">
                                    <input id="first_name" class="form-control" name="first_name" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="middle_name" class="col-md-4 col-form-label text-md-right">{{ __('Отчество') }}</label>
                                <div class="col-md-6">
                                    <input id="middle_name" class="form-control" name="middle_name" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="w_phone" class="col-md-4 col-form-label text-md-right">{{ __('Рабочий телефон') }}</label>
                                <div class="col-md-6">
                                    <input id="w_phone" class="form-control" name="w_phone" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="m_phone" class="col-md-4 col-form-label text-md-right">{{ __('Мобильный телефон') }}</label>
                                <div class="col-md-6">
                                    <input id="m_phone" class="form-control" name="m_phone" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Сохранить') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form action="/logout" method="post">
                            @csrf
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-dark">
                                        {{ __('Выход') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
