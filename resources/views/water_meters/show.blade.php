@extends('templates.bigpicture')
@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-7">{{'Описание водомера: ' . $water_meter->title}}</h2>
        <a href="/water_meters" class="btn btn-sm btn-success" role="button">К списку водомеров</a>
        <a href="/water_meters/{{$water_meter->id + 1}}" class="btn btn-sm btn-success" role="button">Следующий водомер -></a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-primary mb-3">
                    <div class="card-body">
                    <ul>
                        <li>Адрес: {{$water_meter->source->address}}</li>
                        <li>Марка: {{$water_meter->title}}</li>
                        <li>Заводской номер: {{$water_meter->number}}</li>
                        <li>Диаметр: {{$water_meter->diameter}} мм</li>
                        <li>Назначение: {{$water_meter->purpose}}</li>
                        <li>Дата поверки: {{$water_meter->check_date}}</li>
                        <li>Состояние: {{$water_meter->condition}}</li>
                        <li>Примечание: {{$water_meter->note}}</li>
                    </ul>
                    </div>
                @if($water_meter->created_at)
                    <p>Создано: {{$water_meter->updated_at . ' пользователем ' . $water_meter->user->name}}</p>
                @endif
                @if($water_meter->updated_at)
                    <p>Отредактировано: {{$water_meter->updated_at . ' пользователем ' . $water_meter->user->name}}</p>
                @endif
                @if(Auth::user()->role == 'admin')
                <form action="/water_meters/{{$water_meter->id}}" method="post">
                        {{csrf_field()}}
                        @method('delete')
                        <button class="btn btn-sm btn-danger">Удалить</button>
                    </form>
                @endif
                </div>
                @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                    @if($errors->all())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>
                                        {{$error}}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Auth::user()->user_info->user_role == 'admin')
                        <div class="card border-primary mb-3">
                            <h4 class="card-header text-primary">Редактировать водомер</h4>
                            <div class="card-body">
                                <form class ="form-horizontal" action="/water_meters/{{$water_meter->id}}" method="post">
                                    {{csrf_field()}}
                                    @method('put')
                                    <input type="hidden" value="{{$water_meter->id}}" name="id">
                                    <div class="form-group">
                                        <label>Марка:
                                            <input type="text" class="form-control" name="title" value="{{$water_meter->title}}">
                                        </label>
                                        <label>Диаметр
                                            <select name="diameter" class="form-control" required>
                                                <option selected hidden value="{{$water_meter->diameter}}">{{$water_meter->diameter}}</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="25">25</option>
                                                <option value="32">32</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="65">65</option>
                                                <option value="80">80</option>
                                                <option value="100">100</option>
                                            </select>
                                        </label>
                                        <label>Заводской номер:
                                            <input type="text" class="form-control" name="number" value="{{$water_meter->number}}">
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Назначение
                                            <select name="purpose" class="form-control" required>
                                                <option selected hidden value="{{$water_meter->purpose}}">{{$water_meter->purpose}}</option>
                                                <option value="подпитка">подпитка</option>
                                                <option value="хозбыт">хозбыт</option>
                                                <option value="гвс">гвс</option>
                                            </select>
                                        </label>
                                        <label>Котельная
                                            <select name="source_id" class="form-control" required>
                                                <option selected hidden disabled>{{$water_meter->source->address}}</option>
                                                @foreach($water_meter['sources'] as $source)
                                                    <option value="{{$source->id}}">{{$source->address}}</option>
                                                @endforeach
                                            </select>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Состояние
                                            <select name="condition" class="form-control" required>
                                                <option selected hidden value="{{$water_meter->condition}}">{{$water_meter->condition}}</option>
                                                <option value="в работе">в работе</option>
                                                <option value="брак">брак</option>
                                                <option value="резерв">резерв</option>
                                            </select>
                                        </label>
                                        <label>Дата следующей поверки:
                                            <input type="text" class="form-control" name="check_date" value="{{$water_meter->check_date}}">
                                        </label>
                                        <label>Примечание:
                                            <input type="text" class="form-control" name="note" value="{{$water_meter->note}}">
                                        </label>
                                    </div>
                                    <button class="btn btn-success">Сохранить</button>
                                </form>
                            </div>
                        </div>
                    @endif
                    <div class="card border-primary mb-3">
                        <h4 class="card-header text-primary">Внести показания</h4>
                        <div class="card-body">
                            <form class ="form-horizontal text-center" action="/water_meter_values" method="post">
                                @csrf
                                <input type="hidden" name="water_meter_id" value="{{$water_meter->id}}">
                                <div class="form-group">
                                    <label>Месяц:
                                        <input type="month" class="form-control" name="month">
                                    </label>
                                    <label>Показания, м3:
                                        <input type="text" class="form-control" name="value">
                                    </label>
                                    <label>Примечание:
                                        <input type="text" class="form-control" name="note">
                                    </label>
                                    @if(Auth::user()->user_info->user_role == 'admin')
                                    <div class="form-group">
                                        <label>
                                            <input type="checkbox" name="after_check">
                                            показания после поверки</label>
                                    </div>
                                    @endif
                                    <button class="btn btn-success">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @endif
                <div class="card border-primary mb-3">
                    <h4 class="card-header text-primary">Показания</h4>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <tr>
                                    <th>Месяц</th>
                                    <th>Показания, м3</th>
                                    <th>Примечание</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                @foreach($water_meter->water_meter_values as $water_meter_value)
                                    <tr>
                                        <td>{{$water_meter_value->month}}</td>
                                        @if($water_meter_value->after_check == true)
                                        <td>{{$water_meter_value->value}} <span style="color: blue">{{' после поверки'}}</span></td>
                                        @else
                                        <td>{{$water_meter_value->value}}</td>
                                        @endif
                                        <td>{{$water_meter_value->note}}</td>
                                    @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                                            <td>
                                                <a href="/water_meter_values/{{$water_meter_value->id}}/edit" class="btn btn-sm btn-warning" role="button">Изменить</a>
                                            </td>
                                        @endif
                                        @if(Auth::user()->user_info->user_role == 'admin')
                                            <td>
                                                <form action="/water_meter_values/{{$water_meter_value->id}}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-sm btn-danger">X</button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

