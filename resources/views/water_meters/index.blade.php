@extends('templates.bigpicture')

@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">Водомеры</h2>
            <a href="/" class="btn btn-sm btn-success" role="button">На главную</a>
            @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                <a href="/water_meters/create" class="btn btn-sm btn-primary" role="button">Добавить новый</a>
            @endif
        </div>
    </div>
@endsection
@section('content')
@if(count($water_meters['in_work']))
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">В работе</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <tr>
                    <th>№</th>
                    <th>Адрес</th>
                    <th>Марка</th>
                    <th>Диаметр</th>
                    <th>Зав.номер</th>
                    <th>Назначение</th>
                    <th>Дата поверки</th>
                    <th></th>
                    <th></th>
                </tr>
                @php
                    $i = 1;
                @endphp
                @foreach($water_meters['in_work'] as $water_meter)
                    @if ($water_meter->source->in_work == false)
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($water_meter->source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($water_meter->source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <a href="/sources/{{$water_meter->source->slug}}">
                                {{$water_meter->source->address}}
                            </a>
                        </td>
                        <td>
                            <a href="/water_meters/{{$water_meter->id}}">
                                {{$water_meter->title}}
                            </a>
                        </td>
                        <td>{{$water_meter->diameter}}</td>
                        <td>{{$water_meter->number}}</td>
                        <td>{{$water_meter->purpose}}</td>
                        <td>{{$water_meter->check_date}}</td>
                        <td>
                            @if(Auth::user()->user_info->user_role == 'admin' && Auth::user()->user_info->user_level == 10)
                                <form action="/water_meters/{{$water_meter->id}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger">X</button>
                                </form>
                            @endif
                        </td>
                        <td>
                            <a href="/water_meters/{{$water_meter->id}}" class="btn btn-sm btn-success" role="button">Далее</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endif
@if(count($water_meters['reserve']))
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">Резерв</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <tr>
                    <th>№</th>
                    <th>Адрес</th>
                    <th>Марка</th>
                    <th>Диаметр</th>
                    <th>Зав.номер</th>
                    <th>Назначение</th>
                    <th>Дата поверки</th>
                    <th></th>
                    <th></th>
                </tr>
                @php
                    $i = 1;
                @endphp
                @foreach($water_meters['reserve'] as $water_meter)
                    @if ($water_meter->source->in_work == false)
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($water_meter->source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($water_meter->source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <a href="/sources/{{$water_meter->source->slug}}">
                                {{$water_meter->source->address}}
                            </a>
                        </td>
                        <td>
                            <a href="/water_meters/{{$water_meter->id}}">
                                {{$water_meter->title}}
                            </a>
                        </td>
                        <td>{{$water_meter->diameter}}</td>
                        <td>{{$water_meter->number}}</td>
                        <td>{{$water_meter->purpose}}</td>
                        <td>{{$water_meter->check_date}}</td>
                        <td>
                            @if(Auth::user()->user_info->user_role == 'admin' && Auth::user()->user_info->user_level == 10)
                                <form action="/water_meters/{{$water_meter->id}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-sm btn-danger">X</button>
                                </form>
                            @endif
                        </td>
                        <td>
                            <a href="/water_meters/{{$water_meter->id}}" class="btn btn-sm btn-success" role="button">Далее</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endif
@if(count($water_meters['defective']))
<div class="container-fluid">
    <div class="card border-primary mb-3">
        <h4 class="card-header text-primary">Брак</h4>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <tr>
                    <th>№</th>
                    <th>Адрес</th>
                    <th>Марка</th>
                    <th>Диаметр</th>
                    <th>Зав.номер</th>
                    <th>Назначение</th>
                    <th>Дата поверки</th>
                    <th></th>
                    <th></th>
                </tr>
                @php
                    $i = 1;
                @endphp
                @foreach($water_meters['defective'] as $water_meter)
                    @if ($water_meter->source->in_work == false)
                        @continue
                    @endif
                    @if (Auth::user()->user_info->user_role == 'user')
                        @if (Auth::user()->user_info->position == 'мастер')
                            @if ($water_meter->source->master_id != Auth::user()->id)
                                @continue
                            @endif
                        @elseif (Auth::user()->user_info->position == 'старший мастер')
                            @if ($water_meter->source->st_master_id != Auth::user()->id)
                                @continue
                            @endif
                        @else
                            @continue
                        @endif
                    @endif
                    <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <a href="/sources/{{$water_meter->source->slug}}">
                                {{$water_meter->source->address}}
                            </a>
                        </td>
                        <td>
                            <a href="/water_meters/{{$water_meter->id}}">
                                {{$water_meter->title}}
                            </a>
                        </td>
                        <td>{{$water_meter->diameter}}</td>
                        <td>{{$water_meter->number}}</td>
                        <td>{{$water_meter->purpose}}</td>
                        <td>{{$water_meter->check_date}}</td>
                        <td>
                            @if(Auth::user()->user_info->user_role == 'admin' && Auth::user()->user_info->user_level == 10)
                                <form action="/water_meters/{{$water_meter->id}}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-sm btn-danger">X</button>
                            </form>
                            @endif
                        </td>
                        <td>
                            <a href="/water_meters/{{$water_meter->id}}" class="btn btn-sm btn-success" role="button">Далее</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endif
@endsection

