@extends('templates.bigpicture')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/water_meter_report" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Месяц:
                                    <select name="month" class="form-control" required>
                                        @foreach($water_meter_reports as $month)
                                        <option value="{{$month}}">{{$month}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <button class="btn btn-success">Вывести отчет на экран</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/water_meter_report/export" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Месяц:
                                    <select name="month" class="form-control" required>
                                        @foreach($water_meter_reports as $month)
                                            <option value="{{$month}}">{{$month}}</option>
                                        @endforeach
                                    </select>
                                </label>
                                <button class="btn btn-success">Скачать в формате XLS</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
