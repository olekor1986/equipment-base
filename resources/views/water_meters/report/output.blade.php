
@extends('templates.bigpicture')

@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">Отчет по водопотреблению за {{$report['month']}}</h2>
            <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>№</th>
                        <th>Место установки водомера</th>
                        <th>Марка и номер водомера</th>
                        <th>Дата следующей поверки</th>
                        <th>Предыдущие показания</th>
                        <th>Настоящие показания</th>
                        <th>Расход</th>
                        <th>Примечание</th>
                    </tr>
                    @php
                        $i = 1;
                    @endphp
                    @foreach($report['data'] as $item)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$item->address}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{$item->check_date}}</td>
                            <td>{{$item->previous_value}}</td>
                            <td>{{$item->present_value}}
                                @if($item->values_after_check == true)
                                    <span style="color: blue">{{ ' после поверки' }}</span>
                                @endif
                            </td>
                            <td>{{$item->difference}}</td>
                            <td>{{$item->note}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

