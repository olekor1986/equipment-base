@extends('templates.bigpicture')


@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-7">Водомер {{$water_meter->title}}</h2>
            <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/water_meters/{{$water_meter->id}}" method="post">
                            {{csrf_field()}}
                            @method('put')
                            <input type="hidden" value="{{$water_meter->id}}" name="id">
                            <div class="form-group">
                                <label>Марка:
                                    <input type="text" class="form-control" name="title" value="{{$water_meter->title}}">
                                </label>
                                <label>Диаметр
                                    <select name="diameter" class="form-control" required>
                                        <option selected hidden value="{{$water_meter->diameter}}">{{$water_meter->diameter}}</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                        <option value="32">32</option>
                                        <option value="40">40</option>
                                        <option value="50">50</option>
                                        <option value="65">65</option>
                                        <option value="80">80</option>
                                        <option value="100">100</option>
                                    </select>
                                </label>
                                <label>Заводской номер:
                                    <input type="text" class="form-control" name="number" value="{{$water_meter->number}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Назначение
                                    <select name="purpose" class="form-control" required>
                                        <option selected hidden value="{{$water_meter->purpose}}">{{$water_meter->purpose}}</option>
                                        <option value="подпитка">подпитка</option>
                                        <option value="хозбыт">хозбыт</option>
                                        <option value="гвс">гвс</option>
                                    </select>
                                </label>
                                <label>Котельная
                                    <select name="source_id" class="form-control" required>
                                        <option selected hidden disabled>{{$water_meter->source->address}}</option>
                                        @foreach($water_meter['sources'] as $source)
                                            <option value="{{$source->id}}">{{$source->address}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Состояние
                                    <select name="condition" class="form-control" required>
                                        <option selected hidden value="{{$water_meter->condition}}">{{$water_meter->condition}}</option>
                                        <option value="в работе">в работе</option>
                                        <option value="брак">брак</option>
                                        <option value="резерв">резерв</option>
                                    </select>
                                </label>
                                <label>Дата следующей поверки:
                                    <input type="text" class="form-control" name="check_date" value="{{$water_meter->check_date}}">
                                </label>
                                <label>Примечание:
                                    <input type="text" class="form-control" name="note" value="{{$water_meter->note}}">
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
