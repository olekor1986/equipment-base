@extends('templates.bigpicture')


@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-7">Добавить новый водомер</h2>
            <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/water_meters" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Марка:
                                    <input type="text" class="form-control" name="title">
                                </label>
                                <label>Диаметр
                                    <select name="diameter" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="15">15</option>
                                        <option value="20">20</option>
                                        <option value="25">25</option>
                                        <option value="32">32</option>
                                        <option value="40">40</option>
                                        <option value="50">50</option>
                                        <option value="65">65</option>
                                        <option value="80">80</option>
                                        <option value="100">100</option>
                                    </select>
                                </label>
                                <label>Заводской номер:
                                    <input type="text" class="form-control" name="number">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Назначение
                                    <select name="purpose" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="подпитка">подпитка</option>
                                        <option value="хозбыт">хозбыт</option>
                                        <option value="гвс">гвс</option>
                                    </select>
                                </label>
                                <label>Котельная
                                    <select name="source_id" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        @foreach($sources as $source)
                                            <option value="{{$source->id}}">{{$source->address}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Дата следующей поверки:
                                    <input type="text" class="form-control" name="check_date">
                                </label>
                                <label>Первичные показания:
                                    <input type="text" class="form-control" name="primary_value">
                                </label>
                                <label>Примечание:
                                    <input type="text" class="form-control" name="note">
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
