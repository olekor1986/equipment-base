@extends('templates.bigpicture')
@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">О проекте</h2>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <p>
                            Сайт предназначен для управления базой данных оборудования подразделения предприятия.
                            Функционал включает в себя авторизацию пользователей, разграничение доступа пользователей
                            к различным действиям в зависимости от статуса, вывод данных в табличной форме, добавлени,
                            редактирование и удаление данных. Проект продолжает развиваться по мере наличия свободного
                            времени у автора)))
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
