@extends('templates.bigpicture')
@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">Контакты</h2>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <ul>
                            <li>Автор проекта: Коровенко Олег Юрьевич</li>
                            <li>E-mail: olekor1986@gmail.com</li>
                            <li>Phone: 098-992-01-82</li>
                        </ul>
                    </div>
                </div>
                <h6>Мы находися:</h6>
                <div id="map"></div>
                <script type="text/javascript">
                    var map;
                    DG.then(function () {
                        map = DG.map('map', {
                            center: [46.487977, 30.712839],
                            zoom: 13
                        });
                        DG.marker([46.487977, 30.712839]).addTo(map);
                    });
                </script>
            </div>
        </div>
    </div>
@endsection
