@extends('templates.bigpicture')
@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">{{'Характеристики насоса: ' . $pump->title}}</h2>
        <h2 class="display-7">{{$pump->source->type . ' ' . $pump->source->address}}</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                    <ul>
                        <li>Марка - {{$pump->title}}</li>
                        <li>Тип - {{$pump->type}}</li>
                        <li>Напор -  {{$pump->capacity}} м</li>
                        <li>Подача - {{$pump->pressure}} м3/ч</li>
                        <li>Мощность эл.двигателя - {{$pump->engine_power}} кВт</li>
                        <li>Скорость вращения - {{$pump->engine_speed}} об/мин</li>
                        <li>Тип эл.двигателя - {{$pump->engine_type}}</li>
                    </ul>
                    </div>
                </div>
                @if($pump->created_at)
                    <p>Создано: {{$pump->updated_at . ' пользователем ' . $pump->user->name}}</p>
                @endif
                @if($pump->updated_at)
                    <p>Отредактировано: {{$pump->updated_at . ' пользователем ' . $pump->user->name}}</p>
                @endif
                @if(Auth::user()->user_info->user_role == 'admin')
                    <form action="/pumps/{{$pump->id}}" method="post">
                        {{csrf_field()}}
                        @method('delete')
                        <button class="btn btn-sm btn-danger">Удалить</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
