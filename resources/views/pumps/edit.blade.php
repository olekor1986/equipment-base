@extends('templates.bigpicture')
@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">Редактировать данные насоса {{$pump->title}}</h2>
        <h2 class="display-7">{{$pump->source->type . ' ' . $pump->source->address}}</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/pumps/{{$pump->id}}" method="post">
                            {{csrf_field()}}
                            @method('put')
                            <input type="hidden" value="{{$pump->id}}" name="id">
                            <div class="form-group">
                                <label>Марка:
                                    <input type="text" class="form-control" name="title" value="{{$pump->title}}">
                                </label>
                                <label>Тип
                                    <select name="type" class="form-control">
                                        <option selected hidden disabled value="{{$pump->type}}">{{$pump->type}}</option>
                                        <option value="мережевий">мережевий</option>
                                        <option value="циркуляційний">циркуляційний</option>
                                        <option value="підживлюючий">підживлюючий</option>
                                        <option value="підкачка холодної води">підкачка холодної води</option>
                                        <option value="циркуляційний котловий">циркуляційний котловий</option>
                                        <option value="циркуляційний бойлера ГВП">циркуляційний бойлера ГВП</option>
                                        <option value="сирої води">сирої води</option>
                                        <option value="розчину солі">розчину солі</option>
                                        <option value="рециркуляційний котловий">рециркуляційний котловий</option>
                                        <option value="рециркуляційний ГВП">рециркуляційний ГВП</option>
                                        <option value="мережевий ГВП">мережевий ГВП</option>
                                        <option value="живильний">живильний</option>
                                        <option value="конденсатний">конденсатний</option>
                                        <option value="деаераторний">деаераторний</option>
                                        <option value="паровий живильний">паровий живильний</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Адрес:
                                    <select name="source_id" class="form-control">
                                        <option value="{{$pump->source_id}}" selected hidden disabled>{{$pump->source->address}}</option>
                                        @foreach($pump['sources'] as $source)
                                            <option value="{{$source->id}}">{{$source->address}}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Производительность, м3/ч:
                                    <input type="text" class="form-control" name="capacity" value="{{$pump->capacity}}">
                                </label>
                                <label>Напор, м:
                                    <input type="text" class="form-control" name="pressure" value="{{$pump->pressure}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Мощность эл.двигателя, кВт:
                                    <input type="text" class="form-control" name="engine_power" value="{{$pump->engine_power}}">
                                </label>
                                <label>Скорость вращения, об/мин:
                                    <input type="text" class="form-control" name="engine_speed" value="{{$pump->engine_speed}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Тип эл.двигателя:
                                    <input type="text" class="form-control" name="engine_type" value="{{$pump->engine_type}}">
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
