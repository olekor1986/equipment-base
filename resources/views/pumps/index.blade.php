@extends('templates.bigpicture')

@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">Насосы</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
            <a href="/pumps/create" class="btn btn-sm btn-primary" role="button">Добавить новый</a>
        @endif
    </div>
</div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>№</th>
                        <th>Адрес</th>
                        <th>Марка</th>
                        <th>Q, м3/ч</th>
                        <th>Н, м</th>
                        <th>N, кВт</th>
                        <th>n, min-1</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @php
                        $i = 1;
                    @endphp
                    @foreach($pumps as $pump)
                        @if ($pump->source->in_work == false)
                            @continue
                        @endif
                        @if (Auth::user()->user_info->user_role == 'user')
                            @if (Auth::user()->user_info->position == 'мастер')
                                @if ($pump->source->master_id != Auth::user()->id)
                                    @continue
                                @endif
                            @elseif (Auth::user()->user_info->position == 'старший мастер')
                                @if ($pump->source->st_master_id != Auth::user()->id)
                                    @continue
                                @endif
                            @else
                                @continue
                            @endif
                        @endif
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$pump->source->address}}</td>
                            <td>{{$pump->title}}</td>
                            <td>{{$pump->capacity}}</td>
                            <td>{{$pump->pressure}}</td>
                            <td>{{$pump->engine_power}}</td>
                            <td>{{$pump->engine_speed}}</td>
                            <td>
                                <a href="/pumps/{{$pump->id}}" class="btn btn-sm btn-success" role="button">></a>
                            </td>
                            @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/pumps/{{$pump->id}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
