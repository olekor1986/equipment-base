@extends('templates.bigpicture')


@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-7">Котел {{$boiler->title}} №{{$boiler->number}}</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        @if($errors->all())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form class ="form-horizontal" action="/boilers/{{$boiler->id}}" method="post">
                            {{csrf_field()}}
                            @method('put')
                            <input type="hidden" value="{{$boiler->id}}" name="id">
                            <div class="form-group">
                                <label>Марка:
                                    <input type="text" class="form-control" name="title" value="{{$boiler->title}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Цеховой номер:
                                    <input type="text" class="form-control" name="number" value="{{$boiler->number}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Теплоноситель
                                    <select name="energy_carrier" class="form-control">
                                        <option selected hidden value="{{$boiler->energy_carrier}}">Выберите тип...</option>
                                        <option value="вода">Вода</option>
                                        <option value="пар">Пар</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Мощность:
                                    <input type="text" class="form-control" name="power" value="{{$boiler->power}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>КПД:
                                    <input type="text" class="form-control" name="efficient" value="{{$boiler->efficient}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Год выпуска:
                                    <input type="text" class="form-control" name="mount_year" value="{{$boiler->mount_year}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Год ввода в эксплуатацию:
                                    <input type="text" class="form-control" name="launch_year" value="{{$boiler->launch_year}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Заводской номер:
                                    <input type="text" class="form-control" name="serial_number" value="{{$boiler->serial_number}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Регистрационный номер:
                                    <input type="text" class="form-control" name="reg_number" value="{{$boiler->reg_number}}">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Тип горелки
                                    <select name="flame_type" class="form-control">
                                        <option selected hidden value="{{$boiler->flame_type}}">Выберите тип...</option>
                                        <option value="подовий">Подовий</option>
                                        <option value="інжекційна">Iнжекційна</option>
                                        <option value="змішувальний">Змішувальний</option>
                                    </select>
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
