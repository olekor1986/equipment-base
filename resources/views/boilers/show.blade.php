@extends('templates.bigpicture')


@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-7">{{'Описание котла: ' . $boiler->title}}</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
    </div>
</div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-primary mb-3">
                    <div class="card-body">
                    <ul>
                        <li>Марка: {{$boiler->title}}</li>
                        <li>Цеховой номер: {{$boiler->number}}</li>
                        <li>Теплоноситель: {{$boiler->energy_carrier}}</li>
                        <li>Мощность: {{$boiler->power}} Гкал/час</li>
                        <li>КПД: {{$boiler->efficient}}%</li>
                        <li>Год выпуска: {{$boiler->mount_year}}</li>
                        <li>Год ввода в эксплуатацию: {{$boiler->launch_year}}</li>
                        <li>Заводской номер: {{$boiler->serial_number}}</li>
                        <li>Регистрационный номер: {{$boiler->reg_number}}</li>
                        <li>Тип горелки: {{$boiler->flame_type}}</li>
                    </ul>
                    </div>
                </div>
                @if($boiler->created_at)
                    <p>Создано: {{$boiler->updated_at . ' пользователем ' . $boiler->user->name}}</p>
                @endif
                @if($boiler->updated_at)
                    <p>Отредактировано: {{$boiler->updated_at . ' пользователем ' . $boiler->user->name}}</p>
                @endif
                @if(Auth::user()->user_info->user_role == 'admin')
                <form action="/boilers/{{$boiler->id}}" method="post">
                        {{csrf_field()}}
                        @method('delete')
                        <button class="btn btn-sm btn-danger">Удалить</button>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
