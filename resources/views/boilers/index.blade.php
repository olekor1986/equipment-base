@extends('templates.bigpicture')

@section('header')
<div class="header">
    <div class="starter-template">
        <h2 class="display-6">Котлы</h2>
        <a href="{{ URL::previous() }}" class="btn btn-sm btn-success" role="button"><- назад</a>
        @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
        <a href="/boilers/create" class="btn btn-sm btn-primary" role="button">Добавить новый</a>
        @endif
    </div>
</div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>№пп</th>
                        <th>Адрес</th>
                        <th>Марка</th>
                        <th>№</th>
                        <th>Теплоноситель</th>
                        <th>Мощность, Гкал/час</th>
                        <th>КПД, %</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @php
                        $i = 1;
                    @endphp
                    @foreach($boilers as $boiler)
                        @if ($boiler->source->in_work == false)
                            @continue
                        @endif
                        @if (Auth::user()->user_info->user_role == 'user')
                            @if (Auth::user()->user_info->position == 'мастер')
                                @if ($boiler->source->master_id != Auth::user()->id)
                                    @continue
                                @endif
                            @elseif (Auth::user()->user_info->position == 'старший мастер')
                                @if ($boiler->source->st_master_id != Auth::user()->id)
                                    @continue
                                @endif
                            @else
                                @continue
                            @endif
                        @endif
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$boiler->source->address}}</td>
                            <td>{{$boiler->title}}</td>
                            <td>{{$boiler->number}}</td>
                            <td>{{$boiler->energy_carrier}}</td>
                            <td>{{$boiler->power}}</td>
                            <td>{{$boiler->efficient}}</td>
                            <td>
                                <a href="/boilers/{{$boiler->id}}" class="btn btn-sm btn-success" role="button">></a>
                            </td>
                            @if(Auth::user()->user_info->user_role == 'user' || Auth::user()->user_info->user_role == 'admin')
                            <td>
                                <a href="/boilers/{{$boiler->id}}/edit" class="btn btn-sm btn-warning" role="button">Редактировать</a>
                            </td>
                            @endif
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
@endsection
