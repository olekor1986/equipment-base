<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Оборудование - база данных</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">

    <style>
        body {
            background: rgba(115, 135, 139, 0.85) url("/img/bg-image.jpg") no-repeat;
        }
        .admin {
            background: #4a5268;
        }
        nav {
            background-color: rgb(49, 79, 107);
        }
        #dropdown-menu{
            background-color: rgb(49, 79, 107);
        }
        .dropdown-item {
            color: white;
        }
        #logo, #collapse-menu-button {
            color: white;
        }

        .navbar__item {
            color: white;
        }
        .header {
            margin: 10px;
        }
        #homepage-button {
            width: 100%;
        }
        #map {
            margin-top: 15px;
            height: 500px;
        }
        #testbox {
            border: 2px solid black;
            background-color: rgb(49, 79, 107);
            padding: 15px;
        }

        #footer1{
            height: 60px;
            margin-top: 30%;
            padding: 5px;
            background-color: rgb(49, 79, 107);
            color: white;
            font-weight: bolder;
        }

        #footer2 {
            height: 30px;
            background-color: rgb(39, 74, 44);
            font-weight: bolder;
        }

        #footer2__item {
            color: white;
        }
        .guest_message{
            font-family: Arial;
            font-weight: bold;
            font-size: 20px;
            color: #196e0c;
        }
    </style>
    <script src="https://maps.api.2gis.ru/2.0/loader.js"></script>
</head>

<body class="bg-light">
<nav class="navbar navbar-expand-lg navbar-dark">
    <div class="container">
        <a class="navbar-brand" href="/"><img id="logo" src="/img/logo-light.png" height="50px"></a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                data-target="#navbarResponsive"
                aria-controls="navbarResponsive"
                aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="navbar__item nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item active">
                    <a class="navbar__item nav-link" href="/contacts">Контакты</a>
                </li>
                <li class="nav-item active">
                    <a class="navbar__item nav-link" href="/about">О проекте</a>
                </li>
                @if(Auth::check())
                    <li class="nav-item dropdown active">
                        <a class="navbar__item nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">Оборудование</a>
                        <div id="dropdown-menu" class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="/sources">Источники тепла</a>
                            <a class="dropdown-item" href="/boilers">Котлы</a>
                            <a class="dropdown-item" href="/pumps">Насосы</a>
                            <a class="dropdown-item" href="/water_meters">Водомеры</a>
                        </div>
                    </li>
                @endif
            </ul>
            <ul class="navbar-nav dropdup-menu-left">
                @if(Auth::check())
                    <li class="nav-item active">
                        <a class="navbar__item nav-link" href="/user">{{Auth::user()->name}}</a>
                    </li>
                    <form action="/logout" method="post">
                        {{ csrf_field() }}
                        <button class="btn btn-dark">Выход</button>
                    </form>
                @else
                    <li class="nav-item active">
                        <a class="navbar__item nav-link" href="/login">Вход</a>
                    </li>
                    <li class="nav-item active">
                        <a class="navbar__item nav-link" href="/register">Регистрация</a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<section class="header">
    @yield('header')
</section>

<section id="content">
    @yield('content')
</section>

<div id="footer1" class="container-fluid text-center">
    <p>Одесса (с) 2020</p>
</div>
<div id="footer2" class="container-fluid text-center">
    <a id="footer2__item" class="fa fa-facebook" href="https://www.facebook.com/profile.php?id=100017777526184"></a>
    <a id="footer2__item" class="fa fa-at" href="mailto:olekor1986@gmail.com"></a>
    <a id="footer2__item" class="fa fa-telegram" href="https://t.me/OlegKorovenko"></a>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>
