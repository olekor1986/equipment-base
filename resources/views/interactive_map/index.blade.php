@extends('templates.bigpicture')
@section('header')
    <div class="header">
        <div class="starter-template">
            <h2 class="display-6">Объекты на карте</h2>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card border-primary">
                    <div id="map" style="width: 100%; height: 700px;"></div>
                <script type="text/javascript">
                    DG.then(function() {
                        var map,
                            markers = DG.featureGroup(),
                            coordinates = [],
                            labels = [];

                        map = DG.map('map', {
                            center: [46.468630, 30.777505],
                            zoom: 12,
                            zoomSnap: 0.3
                        });

                        @foreach($sources as $source)
                            {{'coordinates[0] = ' . $source->coordinate_0 . ';'}}
                            {{'coordinates[1] = ' . $source->coordinate_1 . ';'}}
                            DG.marker(coordinates).addTo(markers).bindLabel('{{$source->address}}');
                        @endforeach
                        markers.addTo(map);
                        map.fitBounds(markers.getBounds());
                    });

                </script>
            </div>
        </div>
    </div>
@endsection
