@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                @if(Auth::check())
                    <h2 class="mt-5">Добро пожаловать {{Auth::user()->user_info->first_name}}!</h2>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('content')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <a id="homepage-button" href="/water_meter_values/create" role="button" class="btn btn-lg btn-info">
                                Внести показания водомеров
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/water_meter_report" role="button" class="btn btn-lg btn-info">
                                Отчет по воде за месяц
                            </a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p>
                            <a id="homepage-button" href="/monitoring" role="button" class="btn btn-lg btn-success">
                                Мониторинг параметров котельных
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/sources" role="button" class="btn btn-lg btn-info">
                                Источники тепла
                            </a>
                        </p>
                        <!--
                        <p>
                            <a id="homepage-button" href="/interactive_map" role="button" class="btn btn-lg btn-info">
                                Источники тепла на карте
                            </a>
                        </p>
                        -->
                        <p>
                            <a id="homepage-button" href="/boilers" role="button" class="btn btn-lg btn-info">
                                Котлы
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/pumps" role="button" class="btn btn-lg btn-info">
                                Насосы
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/water_meters" role="button" class="btn btn-lg btn-info">
                                Водомеры
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/admin/users" role="button" class="btn btn-lg btn-warning">
                                Users
                            </a>
                        </p>
                        <p>
                            <a id="homepage-button" href="/heating_pipelines" role="button" class="btn btn-lg btn-primary">
                                Теплотрассы
                            </a>
                        </p>
                    </div>
                    </div>
                </div>
            </div>
 @endsection
