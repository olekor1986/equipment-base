@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 class="mt-5">Add new user data</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border-primary mb-3">
                    <div class="card-body">
                        @if($errors->all())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>
                                            {{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form class ="form-horizontal" action="/admin/user_info" method="post">
                            @csrf
                            <input type="hidden" value="{{$user_id}}" name="user_id">
                            <div class="form-group">
                                <label>Фамилия:
                                    <input type="text" class="form-control" name="last_name">
                                </label>
                                <label>Имя:
                                    <input type="text" class="form-control" name="first_name">
                                </label>
                                <label>Отчество:
                                    <input type="text" class="form-control" name="middle_name">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Должность:
                                    <input type="text" class="form-control" name="position">
                                </label>
                                <label>Роль:
                                    <select name="user_role" class="form-control" required>
                                        <option selected hidden disabled>Выберите...</option>
                                        <option value="user">user</option>
                                        <option value="admin">admin</option>
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Тел.моб.:
                                    <input type="text" class="form-control" name="w_phone">
                                </label>
                                <label>Тел.раб.:
                                    <input type="text" class="form-control" name="m_phone">
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Уровень
                                    <input type="text" class="form-control" name="user_level">
                                </label>
                            </div>
                            <button class="btn btn-success">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
