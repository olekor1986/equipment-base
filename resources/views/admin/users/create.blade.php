@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 class="mt-5">Создать нового пользователя</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border-primary mb-3">
                    <div class="card-body">
                        @if($errors->all())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>
                                            {{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <h4>Регистрационные данные</h4>
                        <form class ="form-horizontal" action="/admin/users" method="post">
                            @csrf
                            <div class="form-group">
                                <label>Логин:
                                    <input type="text" class="form-control" name="name">
                                </label>
                                <label>E-mail:
                                    <input type="email" class="form-control" name="email">
                                </label>
                                <label>Password:
                                    <input type="password" class="form-control" name="password">
                                </label>
                            </div>
                            <button class="btn btn-success">Создать</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
