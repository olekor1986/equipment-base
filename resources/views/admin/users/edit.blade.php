@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2 class="mt-5">Данные пользователя {{$user->name}}</h2>
                <a href="/admin/users" role="button" class="btn btn-sm btn-primary">Назад</a>
                <a href="/admin/users/{{$user->id + 1}}/edit" role="button" class="btn btn-sm btn-primary">Следующий</a>
            </div>

        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border-primary mb-3">
                    <div class="card-body">
                            @if($errors->all())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>
                                                {{$error}}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <h4>Регистрационные данные</h4>
                            <form class ="form-horizontal" action="/admin/users/{{$user->id}}" method="post">
                                @csrf
                                @method('put')
                                <input type="hidden" value="{{$user->id}}" name="id">
                                <div class="form-group">
                                    <label>Логин:
                                        <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                    </label>
                                    <label>E-mail:
                                        <input type="email" class="form-control" name="email" value="{{$user->email}}">
                                    </label>
                                </div>
                                <button class="btn btn-success">Сохранить</button>
                            </form>
                            <h4>Дополнительные данные</h4>
                            <form class ="form-horizontal" action="/admin/user_info/{{$user->user_info->id}}" method="post">
                                    @csrf
                                    @method('put')
                                    <input type="hidden" value="{{$user->user_info->id}}" name="id">
                                    <div class="form-group">
                                        <label>Фамилия:
                                            <input type="text" class="form-control" name="last_name" value="{{$user->user_info->last_name}}">
                                        </label>
                                        <label>Имя:
                                            <input type="text" class="form-control" name="first_name" value="{{$user->user_info->first_name}}">
                                        </label>
                                        <label>Отчество:
                                            <input type="text" class="form-control" name="middle_name" value="{{$user->user_info->middle_name}}">
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Должность:
                                            <input type="text" class="form-control" name="position" value="{{$user->user_info->position}}">
                                        </label>
                                        <label>Роль:
                                            <input type="text" class="form-control" name="user_role" value="{{$user->user_info->user_role}}">
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Тел.моб.:
                                            <input type="text" class="form-control" name="w_phone" value="{{$user->user_info->w_phone}}">
                                        </label>
                                        <label>Тел.раб.:
                                            <input type="text" class="form-control" name="m_phone" value="{{$user->user_info->m_phone}}">
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Уровень
                                            <input type="text" class="form-control" name="user_level" value="{{$user->user_info->user_level}}">
                                        </label>
                                        <label>userId
                                            <input type="text" class="form-control" name="user_id" value="{{$user->user_info->user_id}}">
                                        </label>
                                    </div>
                                    <button class="btn btn-success">Сохранить</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
