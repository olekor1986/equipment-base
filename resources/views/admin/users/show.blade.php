@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Данные пользователя {{$user->name}}</h2>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card border-primary mb-3">
                    <div class="card-body">
                        <ul>
                            <li>Логин: {{$user->name}}</li>
                            <li>E-mail: {{$user->email}}</li>
                            <li>ФИО:
                                {{$user->user_info->last_name . ' '}}
                                {{$user->user_info->first_name . ' '}}
                                {{$user->user_info->middle_name}}
                            </li>
                            <li>Должность: {{$user->user_info->position}}</li>
                            <li>Роль: {{$user->user_info->user_role}}</li>
                            <li>Рабочий тел.: {{$user->user_info->w_phone}}</li>
                            <li>Мобильный тел.: {{$user->user_info->m_phone}}</li>
                            <li>Уровень: {{$user->user_info->user_level}}</li>
                            <li>userID: {{$user->user_info->user_id}}</li>
                        </ul>
                    </div>
                </div>
                <form action="/admin/users/{{$user->id}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="btn btn-sm btn-danger">Удалить</button>
                </form>
            </div>
        </div>
    </div>
@endsection
