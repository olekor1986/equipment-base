@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h2 class="mt-5">Пользователи</h2>
                <a href="/admin/users/create" class="btn btn-sm btn-primary" role="button">Add new user</a>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <tr>
                        <th>№</th>
                        <th>Логин</th>
                        <th>E-mail</th>
                        <th>ФИО</th>
                        <th>Должность</th>
                        <th>Роль</th>
                        <th>userID</th>
                        <th></th>
                        <th></th>
                    </tr>
                    @foreach($users as $user)
                        @if(!isset($user->user_info->user_id))
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <form action="/admin/users/{{$user->id}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-sm btn-danger">X</button>
                                    </form>
                                </td>
                                <td>
                                    <a href="/admin/user_info/{{$user->id}}/create" class="btn btn-sm btn-warning" role="button">Add user info</a>
                                </td>
                            </tr>

                    @else
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                {{$user->user_info->last_name . ' '}}
                                {{$user->user_info->first_name . ' '}}
                                {{$user->user_info->middle_name}}
                            </td>
                            <td>{{$user->user_info->position}}</td>
                            <td>{{$user->user_info->user_role}}</td>
                            <td>{{$user->user_info->user_id}}</td>
                            <td>
                                <a href="/admin/users/{{$user->id}}" class="btn btn-sm btn-success" role="button">></a>
                            </td>
                            <td>
                                <a href="/admin/users/{{$user->id}}/edit" class="btn btn-sm btn-warning" role="button">Edit</a>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
 @endsection
