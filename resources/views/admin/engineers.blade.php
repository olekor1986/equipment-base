@extends('templates.admin')
@section('header')
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                @if(Auth::check())
                    <h2 class="mt-5">Инженерный персонал</h2>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <a id="homepage-button" href="/master" role="button" class="btn btn-lg btn-outline-dark">
                        Мастера
                    </a>
                </p>
                <p>
                    <a id="homepage-button" href="/st_master" role="button" class="btn btn-lg btn-outline-dark">
                        Старшие мастера
                    </a>
                </p>
                <p>
                    <a id="homepage-button" href="/el_master" role="button" class="btn btn-lg btn-outline-dark">
                        Мастера-электрики
                    </a>
                </p>
                <p>
                    <a id="homepage-button" href="/el_mounter" role="button" class="btn btn-lg btn-outline-dark">
                        Электромонтеры
                    </a>
                </p>
                <p>
                    <a id="homepage-button" href="/kip_master" role="button" class="btn btn-lg btn-outline-dark">
                        Мастера КИПиА
                    </a>
                </p>
                <p>
                    <a id="homepage-button" href="/kip_mounter" role="button" class="btn btn-lg btn-outline-dark">
                        Слесаря КИПиА
                    </a>
                </p>
            </div>
        </div>
    </div>
@endsection

