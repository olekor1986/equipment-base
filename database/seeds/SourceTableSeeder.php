<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Library\CsvDataLoader;

/**
 * Class ProductsTableSeeder
 */
class SourceTableSeeder extends Seeder
{

    /**
     * @var string
     */
    protected $filename = 'sources.csv';
    /**
     * @var array
     */
    private $sources = [];

    /**
     * The method iterates over the source array and adds the 'slug' element based on the 'title'
     * @param array $array
     * @return array
     */
    private function slugged(array $array):array
    {
        $result = [];
        foreach ($array as $key => $value){
            $result[$key]['address'] = $value['address'];
            $result[$key]['slug'] = Str::slug($value['address'], '-');
            $result[$key]['type'] = $value['type'];
            $result[$key]['fuel'] = $value['fuel'];
            $result[$key]['full_power'] = floatval($value['full_power']);
            $result[$key]['connected_power'] = floatval($value['connected_power']);
            $result[$key]['district'] = $value['district'];
            $result[$key]['gps'] = $value['gps'];
            $result[$key]['master_id'] = intval($value['master_id']);
            $result[$key]['st_master_id'] = intval($value['st_master_id']);
            $result[$key]['el_mounter_id'] = intval($value['electric_id']);
            $result[$key]['el_master_id'] = intval($value['electric_master_id']);
            $result[$key]['kip_mounter_id'] = intval($value['kip_id']);
            $result[$key]['kip_master_id'] = intval($value['kip_master_id']);
            $result[$key]['user_id'] = intval($value['user_id']);
            $result[$key]['in_work'] = boolval($value['in_work']);
        }
        return $result;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->sources = CsvDataLoader::csv_to_array($this->filename);
        } catch (Exception $e) {
            $message = "Can't open file! " . $e->getMessage();
            die($message);
        }

        DB::table('sources')->insert($this->slugged($this->sources));
    }
}
