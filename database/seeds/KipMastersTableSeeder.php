<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KipMastersTableSeeder extends Seeder
{
    protected $kipMasters = [
        [
            'name' => 'Бабий Сергей Андреевич',
            'w_phone' => '794-87-45',
            'm_phone' => '068-105-41-23'
        ],
        [
            'name' => 'Урсатий Анатолий Александрович',
            'w_phone' => '794-68-01',
            'm_phone' => '097-617-35-37'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('kip_masters')->insert($this->kipMasters );
    }
}
