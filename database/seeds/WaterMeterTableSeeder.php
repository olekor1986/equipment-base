<?php

use App\Library\CsvDataLoader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WaterMeterTableSeeder extends Seeder
{
    /**
     * @var string
     */
    protected $filename = 'water_meters.csv';
    /**
     * @var array
     */
    private $waterMeters = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->waterMeters = CsvDataLoader::csv_to_array($this->filename);
        } catch (Exception $e) {
            $message = "Can't open file! " . $e->getMessage();
            die($message);
        }

        $result = [];
        foreach ($this->waterMeters as $key => $value){
            $result[$key]['title'] = $value['title'];
            $result[$key]['diameter'] = $value['diameter'];
            $result[$key]['number'] = $value['number'];
            $result[$key]['purpose'] = $value['purpose'];
            $result[$key]['check_date'] = $value['check_date'];
            $result[$key]['source_id'] = intval($value['source_id']);
            $result[$key]['note'] = $value['note'];
        }
        DB::table('water_meters')->insert($result);
    }
}
