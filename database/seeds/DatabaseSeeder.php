<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*
        $this->call(UsersTableSeeder::class);
        $this->call(BoilersTableSeeder::class);
        $this->call(PumpsTableSeeder::class);
        $this->call(SourceTableSeeder::class);
        $this->call(MastersTableSeeder::class);
        $this->call(StMastersTableSeeder::class);
        $this->call(ElMountersTableSeeder::class);
        $this->call(ElMastersTableSeeder::class);
        $this->call(KipMountersTableSeeder::class);
        $this->call(KipMastersTableSeeder::class);
        $this->call(WaterMeterTableSeeder::class);
        $this->call(WaterMeterValuesTableSeeder::class);
        $this->call(UserInfoTableSeeder::class);
        */
    }
}
