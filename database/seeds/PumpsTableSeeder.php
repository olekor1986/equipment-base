<?php

use App\Library\CsvDataLoader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PumpsTableSeeder extends Seeder
{

    /**
     * @var string
     */
    protected $filename = 'pumps_base.csv';
    /**
     * @var array
     */
    private $pumps = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->pumps = CsvDataLoader::csv_to_array($this->filename);
        } catch (Exception $e) {
            $message = "Can't open file! " . $e->getMessage();
            die($message);
        }

        $result = [];
        foreach ($this->pumps as $key => $value){
            $result[$key]['type'] = $value['type'];
            $result[$key]['title'] = $value['title'];
            $result[$key]['capacity'] = $value['capacity'];
            $result[$key]['pressure'] = $value['pressure'];
            $result[$key]['engine_power'] = $value['engine_power'];
            $result[$key]['engine_speed'] = $value['engine_speed'];
            $result[$key]['engine_type'] = $value['engine_type'];
            $result[$key]['source_id'] = intval($value['source_id']);
            $result[$key]['user_id'] = intval($value['user_id']);
        }

        DB::table('pumps')->insert($result);
    }
}
