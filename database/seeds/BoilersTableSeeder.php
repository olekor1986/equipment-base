<?php

use App\Library\CsvDataLoader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BoilersTableSeeder extends Seeder
{

    /**
     * @var string
     */
    protected $filename = 'boilers_base.csv';
    /**
     * @var array
     */
    private $boilers = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->boilers = CsvDataLoader::csv_to_array($this->filename);
        } catch (Exception $e) {
            $message = "Can't open file! " . $e->getMessage();
            die($message);
        }

        $result = [];
        foreach ($this->boilers as $key => $value){
            $result[$key]['title'] = $value['title'];
            $result[$key]['number'] = $value['number'];
            $result[$key]['energy_carrier'] = $value['energy_carrier'];
            $result[$key]['power'] = floatval($value['power']);
            $result[$key]['efficient'] = floatval($value['efficient']);
            $result[$key]['mount_year'] = $value['mount_year'];
            $result[$key]['launch_year'] = $value['launch_year'];
            $result[$key]['serial_number'] = $value['serial_number'];
            $result[$key]['reg_number'] = $value['reg_number'];
            $result[$key]['flame_type'] = $value['flame_type'];
            $result[$key]['source_id'] = intval($value['source_id']);
            $result[$key]['user_id'] = intval($value['user_id']);
        }
        DB::table('boilers')->insert($result);
    }
}
