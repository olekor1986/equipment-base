<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ElMountersTableSeeder extends Seeder
{
    protected $elMounters = [
        [
            'name' => 'Электромонтер_1',
            'w_phone' => '777-00-00',
            'm_phone' => '099-999-99-99'
        ],
        [
            'name' => 'Электромонтер_2',
            'w_phone' => '777-00-00',
            'm_phone' => '099-999-99-99'
        ],
        [
            'name' => 'Электромонтер_3',
            'w_phone' => '777-00-00',
            'm_phone' => '099-999-99-99'
        ],
        [
            'name' => 'Электромонтер_4',
            'w_phone' => '777-00-00',
            'm_phone' => '099-999-99-99'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('el_mounters')->insert($this->elMounters );
    }
}
