<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KipMountersTableSeeder extends Seeder
{
    protected $kipMounters = [
        [
            'name' => 'Корся Игорь Владимирович',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Трач Александр Иосифович',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Турышев Юрий Витальевич',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Васильев Борис Георгиевич',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Головатенко Владимир Николаевич',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Головин Александр Олегович',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Золотухин Валерий Тимофеевич',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Кащук Валерий Иванович',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Кормильцев Олег Леонидович',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Печерский Николай Анатольевич',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Батечко',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Руденко Олег Игоревич',
            'w_phone' => '',
            'm_phone' => ''
        ],
        [
            'name' => 'Силаев Юрий Николаевич',
            'w_phone' => '',
            'm_phone' => ''
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('kip_mounters')->insert($this->kipMounters );
    }
}
