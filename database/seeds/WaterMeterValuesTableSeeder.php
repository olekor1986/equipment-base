<?php

use App\Library\CsvDataLoader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WaterMeterValuesTableSeeder extends Seeder
{
    /**
     * @var string
     */
    protected $filename = 'water_meters_values.csv';
    /**
     * @var array
     */
    private $waterMetersValues = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->waterMetersValues = CsvDataLoader::csv_to_array($this->filename);
        } catch (Exception $e) {
            $message = "Can't open file! " . $e->getMessage();
            die($message);
        }

        $result = [];
        foreach ($this->waterMetersValues as $key => $value){
            $result[$key]['water_meter_id'] = intval($value['water_meter_id']);
            $result[$key]['month'] = $value['month'];
            $result[$key]['value'] = $value['value'];
           }
        DB::table('water_meter_values')->insert($result);
    }
}
