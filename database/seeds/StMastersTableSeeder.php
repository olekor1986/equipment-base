<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StMastersTableSeeder extends Seeder
{
    protected $stMasters = [
        [
            'name' => 'Шерстюк Леонид Николаевич',
            'w_phone' => '701-32-35',
            'm_phone' => '068-254-62-77'
        ],
        [
            'name' => 'Шаларь Юрий Андреевич',
            'w_phone' => '30-90-53',
            'm_phone' => '067-589-79-32'
        ],
        [
            'name' => 'Бобраков Василий Дмитриевич',
            'w_phone' => '799-05-92',
            'm_phone' => '098-955-59-08'
        ],
        [
            'name' => 'Тушков Константин Анатольевич',
            'w_phone' => '701-32-24',
            'm_phone' => '067-281-90-37'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('st_masters')->insert($this->stMasters);
    }
}
