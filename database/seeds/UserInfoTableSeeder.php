<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserInfoTableSeeder extends Seeder
{
    protected $user_info = [
        [
            'first_name' => 'Олег',
            'middle_name' => 'Юрьевич',
            'last_name' => 'Коровенко',
            'position' => 'старший мастер',
            'w_phone' => '048-794-89-48',
            'm_phone' => '098-992-01-82',
            'user_level' => 10,
            'user_role' => 'admin',
            'banned' => false,
            'user_id' => 1
        ],
        [
            'first_name' => 'Павел',
            'middle_name' => 'Владимирович',
            'last_name' => 'Черноус',
            'position' => 'мастер',
            'w_phone' => '30-90-68',
            'm_phone' => '068-255-28-90',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 2
        ],
        [
            'first_name' => 'Виталий',
            'middle_name' => 'Дмитриевич',
            'last_name' => 'Калиниченко',
            'position' => 'мастер',
            'w_phone' => '701-32-21',
            'm_phone' => '068-256-72-66',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 3
        ],
        [
            'first_name' => 'Сергей',
            'middle_name' => 'Юрьевич',
            'last_name' => 'Задорожный',
            'position' => 'мастер',
            'w_phone' => '794-63-03',
            'm_phone' => '096-930-60-05',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 4
        ],
        [
            'first_name' => 'Олег',
            'middle_name' => 'Анатольевич',
            'last_name' => 'Сливка',
            'position' => 'мастер',
            'w_phone' => '701-32-21',
            'm_phone' => '067-708-53-37',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 5
        ],
        [
            'first_name' => 'Сергей',
            'middle_name' => 'Васильевич',
            'last_name' => 'Оковитый',
            'position' => 'мастер',
            'w_phone' => '701-32-39',
            'm_phone' => '097-374-02-52',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 6
        ],
        [
            'first_name' => 'Сергей',
            'middle_name' => 'Владимирович',
            'last_name' => 'Захаренко',
            'position' => 'мастер',
            'w_phone' => '798-62-11',
            'm_phone' => '093-225-20-08',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 7
        ],
        [
            'first_name' => 'Сергей',
            'middle_name' => 'Владимирович',
            'last_name' => 'Самарцев',
            'position' => 'мастер',
            'w_phone' => '794-89-61',
            'm_phone' => '096-499-63-95',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 8
        ],
        [
            'first_name' => 'Александр',
            'middle_name' => 'Ефимович',
            'last_name' => 'Подоусов',
            'position' => 'мастер',
            'w_phone' => '701-32-45',
            'm_phone' => '097-374-02-52',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 9
        ],
        [
            'first_name' => 'Олег',
            'middle_name' => 'Васильевич',
            'last_name' => 'Финьковский',
            'position' => 'мастер',
            'w_phone' => '794-89-11',
            'm_phone' => '063-822-11-48',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 10
        ],
        [
            'first_name' => 'Валентин',
            'middle_name' => 'Олегович',
            'last_name' => 'Костев',
            'position' => 'мастер',
            'w_phone' => '702-00-48',
            'm_phone' => '063-679-83-36',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 11
        ],
        [
            'first_name' => 'Александр',
            'middle_name' => 'Станиславович',
            'last_name' => 'Таранюк',
            'position' => 'мастер',
            'w_phone' => '787-46-23',
            'm_phone' => '067-695-96-11',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 12
        ],
        [
            'first_name' => 'Тамара',
            'middle_name' => 'Алексеевна',
            'last_name' => 'Иванченко',
            'position' => 'мастер',
            'w_phone' => '794-89-60',
            'm_phone' => '067-928-09-72',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 13
        ],
        [
            'first_name' => 'Журакул',
            'middle_name' => 'Содатович',
            'last_name' => 'Содатов',
            'position' => 'мастер',
            'w_phone' => '701-71-44',
            'm_phone' => '097-747-58-99',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 14
        ],
        [
            'first_name' => 'Евгений',
            'middle_name' => 'Васильевич',
            'last_name' => 'Молоканов',
            'position' => 'мастер',
            'w_phone' => '702-22-18',
            'm_phone' => '095-938-19-35',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 15
        ],
        [
            'first_name' => 'Юрий',
            'middle_name' => 'Викторович',
            'last_name' => 'Дмитренко',
            'position' => 'мастер',
            'w_phone' => '743-72-38',
            'm_phone' => '050-928-97-94',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 16
        ],
        [
            'first_name' => 'Василий',
            'middle_name' => 'Несторович',
            'last_name' => 'Парыгин',
            'position' => 'мастер',
            'w_phone' => '701-32-29',
            'm_phone' => '067-556-12-75',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 17
        ],
        [
            'first_name' => 'Юрий',
            'middle_name' => 'Викторович',
            'last_name' => 'Бондаренко',
            'position' => 'мастер',
            'w_phone' => '',
            'm_phone' => '050-390-78-53',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 18
        ],
        [
            'first_name' => 'Леонид',
            'middle_name' => 'Николаевич',
            'last_name' => 'Шерстюк',
            'position' => 'старший мастер',
            'w_phone' => '701-32-35',
            'm_phone' => '068-254-62-77',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 19
        ],
        [
            'first_name' => 'Юрий',
            'middle_name' => 'Андреевич',
            'last_name' => 'Шаларь',
            'position' => 'старший мастер',
            'w_phone' => '30-90-53',
            'm_phone' => '067-589-79-32',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 20
        ],
        [
            'first_name' => 'Василий',
            'middle_name' => 'Дмитриевич',
            'last_name' => 'Бобраков',
            'position' => 'старший мастер',
            'w_phone' => '799-05-92',
            'm_phone' => '098-955-59-08',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 21
        ],
        [
            'first_name' => 'Константин',
            'middle_name' => 'Анатольевич',
            'last_name' => 'Тушков',
            'position' => 'старший мастер',
            'w_phone' => '701-32-24',
            'm_phone' => '067-281-90-37',
            'user_level' => 10,
            'user_role' => 'user',
            'banned' => false,
            'user_id' => 22
        ],
        [
            'first_name' => 'Костева',
            'middle_name' => 'Анна',
            'last_name' => 'Юрьевна',
            'position' => 'инженер',
            'w_phone' => '',
            'm_phone' => '',
            'user_level' => 10,
            'user_role' => 'admin',
            'banned' => false,
            'user_id' => 23
        ],
        [
            'first_name' => 'Киченок',
            'middle_name' => 'Ольга',
            'last_name' => 'Сергеевна',
            'position' => 'инженер',
            'w_phone' => '',
            'm_phone' => '',
            'user_level' => 10,
            'user_role' => 'admin',
            'banned' => false,
            'user_id' => 24
        ],
        [
            'first_name' => 'FirstName',
            'middle_name' => 'MiddleName',
            'last_name' => 'LastName',
            'position' => 'tester',
            'w_phone' => '777-77-77',
            'm_phone' => '555-55-55',
            'user_level' => 1,
            'user_role' => 'guest',
            'banned' => false,
            'user_id' => 25
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_info')->insert($this->user_info);
    }
}
