<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ElMastersTableSeeder extends Seeder
{
    protected $elMasters = [
        [
            'name' => 'Бовтрюк Людмила Вильевна',
            'w_phone' => '702-00-83',
            'm_phone' => '068-934-63-60'
        ],
        [
            'name' => 'Савельев Александр Олегович',
            'w_phone' => '30-90-71',
            'm_phone' => '098-258-78-71'
        ],
        [
            'name' => 'Лукьянов Роман Олегович',
            'w_phone' => '794-85-26',
            'm_phone' => '096-762-05-48'
        ],
        [
            'name' => 'Чистяков Александр Владимирович',
            'w_phone' => '704-31-90',
            'm_phone' => '096-557-95-26'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('el_masters')->insert($this->elMasters);
    }
}
