<?php

use App\Library\CsvDataLoader;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeatingPipelinesTableSeeder extends Seeder
{
    /**
     * @var string
     */
    protected $filename = 'heating_pipelines.csv';
    /**
     * @var array
     */
    private $pumps = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $this->pumps = CsvDataLoader::csv_to_array($this->filename);
        } catch (Exception $e) {
            $message = "Can't open file! " . $e->getMessage();
            die($message);
        }

        $result = [];
        foreach ($this->pumps as $key => $value){
            $result[$key]['source_id'] = intval($value['source_id']);
            $result[$key]['pipe_start'] = strval($value['pipe_start']);
            $result[$key]['pipe_end'] = strval($value['pipe_end']);
            $result[$key]['direct_diam'] = intval($value['direct_diam']);
            $result[$key]['reverse_diam'] = intval($value['reverse_diam']);
            $result[$key]['length'] = floatval($value['length']);
            $result[$key]['type'] = strval($value['type']);
            $result[$key]['method'] = strval($value['method']);
            $result[$key]['ins_type'] = strval($value['ins_type']);
            $result[$key]['ins_cond'] = strval($value['ins_cond']);
            $result[$key]['ins_thick'] = intval($value['ins_thick']);
            $result[$key]['height'] = floatval($value['height']);
            $result[$key]['build_year'] = strval($value['build_year']);
            $result[$key]['note'] = '';
            $result[$key]['user_id'] = 1;
        }

        DB::table('heating_pipelines')->insert($result);
    }
}
