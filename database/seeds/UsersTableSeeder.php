<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    protected $users = [
        [
            'name' => 'o.korovenko',
            'email' => 'olekor1986@gmail.com',
            'password' => '123456789Qw'
        ],
        [
            'name' => 'p.chernous',
            'email' => 'group.center1@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'v.kalinichenko',
            'email' => 'group.center2@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 's.zadorojny',
            'email' => 'group.center3@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'o.slivka',
            'email' => 'group.center4@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 's.okovitij',
            'email' => 'group.south1@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 's.zaharenko',
            'email' => 'group.south2@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 's.samarcev',
            'email' => 'group.south3@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'a.podousov',
            'email' => 'group.south4@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'o.finkovsky',
            'email' => 'group.north1@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'v.kostev',
            'email' => 'group.north2@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'a.taraniuk',
            'email' => 'group.north3@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 't.ivanchenko',
            'email' => 'group.north4@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'j.sodatov',
            'email' => 'group.middle1@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'e.molokanov',
            'email' => 'group.middle2@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'y.dmitrenko',
            'email' => 'group.middle3@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'v.parygin',
            'email' => 'group.middle4@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'y.bondarenko',
            'email' => 'group.middle5@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'l.sherstiuk',
            'email' => 'group.center0@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'y.shalar',
            'email' => 'group.south0@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'v.bobrakov',
            'email' => 'group.north0@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'k.tushkov',
            'email' => 'group.middle0@mail.com',
            'password' => '123456Qw'
        ],
        [
            'name' => 'a.kosteva',
            'email' => 'engineer.anya@mail.com',
            'password' => '12345eee'
        ],
        [
            'name' => 'o.kichenok',
            'email' => 'engineer.olga@mail.com',
            'password' => '12345eee'
        ],
        [
            'name' => 'TestUser',
            'email' => 'test@mail.com',
            'password' => '1234567890'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $array = [];
        foreach ($this->users as $key => $value) {
            $array[$key]['name'] = $value['name'];
            $array[$key]['email'] = $value['email'];
            $array[$key]['password'] = Hash::make($value['password']);
        }
        DB::table('users')->insert($array);
    }
}
