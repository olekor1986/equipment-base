<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaterMeterReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('water_meter_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('water_meter_id');
            $table->string('address');
            $table->string('title');
            $table->string('check_date')->nullable();
            $table->string('previous_value')->default('00000');
            $table->string('present_value')->default('00000');
            $table->string('difference')->default('0');
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('water_meter_reports');
    }
}
