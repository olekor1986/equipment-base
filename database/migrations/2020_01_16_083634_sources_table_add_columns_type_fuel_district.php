<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SourcesTableAddColumnsTypeFuelDistrict extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sources', function (Blueprint $table){
            $table->string('type')->after('address');
            $table->string('fuel')->after('address');
            $table->string('district')->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sources', function (Blueprint $table){
            $table->dropColumn('type');
            $table->dropColumn('fuel');
            $table->dropColumn('district');
        });
    }
}
