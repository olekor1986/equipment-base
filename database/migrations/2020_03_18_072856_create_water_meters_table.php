<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaterMetersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('water_meters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->default('Водомер');
            $table->string('diameter')->default('15');
            $table->string('number')->nullable();
            $table->string('purpose')->nullable();
            $table->string('check_date')->nullable();
            $table->integer('source_id');
            $table->integer('user_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('water_meters');
    }
}
