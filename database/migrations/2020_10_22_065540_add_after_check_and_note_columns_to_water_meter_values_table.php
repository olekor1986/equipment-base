<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAfterCheckAndNoteColumnsToWaterMeterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('water_meter_values', function (Blueprint $table) {
            $table->boolean('after_check')->after('value')->default(false);
            $table->string('note')->after('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('water_meter_values', function (Blueprint $table) {
            $table->dropColumn('after_check');
            $table->dropColumn('note');
        });
    }
}
