<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeatingPipelineRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heating_pipeline_repairs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('heating_pipeline_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('diameter')->nullable();
            $table->text('workload');
            $table->string('gps')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heating_pipeline_repairs');
    }
}
