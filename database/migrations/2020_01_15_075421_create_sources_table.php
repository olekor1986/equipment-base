<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address');
            $table->string('slug');
            $table->string('type');
            $table->integer('type_id');
            $table->integer('fuel_id');
            $table->integer('address_id');
            $table->float('full_power');
            $table->float('connected_power');
            $table->integer('district_id');
            $table->float('n_value');
            $table->float('e_value');
            $table->integer('master_id');
            $table->integer('st_master_id');
            $table->integer('el_mounter_id');
            $table->integer('el_master_id');
            $table->integer('kip_mounter_id');
            $table->integer('kip_master_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sources');
    }
}
