<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEngineersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('w_phone');
            $table->string('m_phone');
            $table->string('tab_number')->nullable();
            $table->string('st_master_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('st_masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('w_phone');
            $table->string('m_phone');
            $table->string('tab_number')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('el_mounters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('w_phone');
            $table->string('m_phone');
            $table->string('tab_number')->nullable();
            $table->string('el_master_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('el_masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('w_phone');
            $table->string('m_phone');
            $table->string('tab_number')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('kip_mounters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('w_phone');
            $table->string('m_phone');
            $table->string('tab_number')->nullable();
            $table->string('kip_master_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
        Schema::create('kip_masters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('w_phone');
            $table->string('m_phone');
            $table->string('tab_number')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters');
        Schema::dropIfExists('st_masters');
        Schema::dropIfExists('el_mounters');
        Schema::dropIfExists('el_masters');
        Schema::dropIfExists('kip_mounters');
        Schema::dropIfExists('kip_masters');

    }
}
