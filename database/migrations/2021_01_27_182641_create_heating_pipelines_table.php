<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeatingPipelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heating_pipelines', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('source_id');
            $table->string('pipe_start')->nullable();
            $table->string('pipe_end')->nullable();
            $table->integer('direct_diam')->nullable();
            $table->integer('reverse_diam')->nullable();
            $table->float('length')->nullable();
            $table->string('type');
            $table->string('method');
            $table->string('ins_type');
            $table->string('ins_cond')->nullable();
            $table->integer('ins_thick')->nullable();
            $table->float('height')->nullable();
            $table->string('build_year')->nullable();
            $table->string('note')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heating_pipelines');
    }
}
