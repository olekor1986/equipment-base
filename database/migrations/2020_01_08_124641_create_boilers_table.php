<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoilersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boilers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('number');
            $table->string('energy_carrier');
            $table->float('power');
            $table->float('efficient');
            $table->string('mount_year');
            $table->string('launch_year');
            $table->string('serial_number');
            $table->string('reg_number');
            $table->string('flame_type');
            $table->integer('source_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boilers');
    }
}
