<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SourcesTableRemoveColumnsNValueEValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sources', function(Blueprint $table){
            $table->dropColumn('n_value');
            $table->dropColumn('e_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sources', function(Blueprint $table){
            $table->float('n_value')->after('connected_power')->nullable();
            $table->float('e_value')->after('connected_power')->nullable();
        });
    }
}
