<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/contacts', function(){ return view('contacts.index'); });

Route::get('/about', function(){ return view('about.index'); });

Route::resource('sources', 'SourcesController');

Route::resource('boilers', 'BoilerController');

Route::resource('pumps', 'PumpsController');

Route::resource('water_meters', 'WaterMeterController')->only('index', 'create',
    'store', 'show', 'update', 'destroy');

Route::get('/water_meter_values/rebuild', 'WaterMeterValuesController@monthlyCheckValues');

Route::resource('water_meter_values', 'WaterMeterValuesController');

Route::get('/water_meter_report', 'WaterMeterReportController@index');

Route::post('/water_meter_report', 'WaterMeterReportController@report');

Route::post('/water_meter_report/export', 'WaterMeterReportController@export');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('user', 'UserController');

Route::resource('master', 'Engineers\MasterController');

Route::resource('/user_info', 'UserInfoController');

/*** Admin routes ***/
Route::get('/admin', 'Admin\AdminController@index');
Route::get('/engineers', 'Admin\AdminController@engineers');

/*** Admin User routes ***/
Route::resource('/admin/users', 'Admin\AdminUserController');

/*** Admin UserInfo routes ***/
Route::get('/admin/user_info/{user_id}/create', 'Admin\AdminUserInfoController@create');
Route::resource('/admin/user_info','Admin\AdminUserInfoController');
/*
Route::get('/admin/user_info', 'Admin\AdminUserInfoController@index');
Route::get('/admin/user_info/{user_id}/create', 'Admin\AdminUserInfoController@create');
Route::post('/admin/user_info', 'Admin\AdminUserInfoController@store');
Route::get('/admin/user_info/{user_info}/edit', 'Admin\AdminUserInfoController@edit');
Route::put('/admin/user_info/{user_info}', 'Admin\AdminUserInfoController@update');
Route::delete('/admin/user_info/{user_info}', 'Admin\AdminUserInfoController@destroy');
*/
/*** Interactive Map ***/
//Route::resource('/interactive_map','InteractiveMapController');

/*** Heating Pipelines ***/
Route::resource('heating_pipelines','HeatingPipelineController');

/*** Heating Pipeline Repairs ***/
Route::resource('heating_pipeline_repairs','HeatingPipelineRepairController');

Route::get('/monitoring', 'MonitoringController@index');
