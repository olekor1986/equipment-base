<?php

namespace App;
use App\Engineers\ElMaster;
use App\Engineers\ElMounter;
use App\Engineers\KipMaster;
use App\Engineers\KipMounter;
use App\Engineers\Master;
use App\Engineers\StMaster;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Source extends Model
{
    protected $fillable = [
            'id', 'address', 'slug', 'type', 'fuel', 'full_power', 'connected_power',
            'district', 'gps', 'master_id', 'st_master_id',
            'el_mounter_id', 'el_master_id', 'kip_mounter_id', 'kip_master_id',
            'user_id', 'in_work', 'monitoring'
        ];

    protected $data_sources;
    protected $user_id;
    protected $employees = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getSourceEmployees()
    {
        $this->employees['masters'] = User::all()
            ->load(['user_info' => function ($query) {
                $query->where('user_role', 'like', 'user');
                $query->where('position', 'like', 'мастер');
            }]);
        $this->employees['st_masters'] = User::all()
            ->load(['user_info' => function ($query) {
                $query->where('user_role', 'like', 'user');
                $query->where('position', 'like', 'старший мастер');
            }]);
        $this->employees['el_mounters'] = ElMounter::all()->sortBy('name');
        $this->employees['el_masters'] = ElMaster::all()->sortBy('name');
        $this->employees['kip_mounters'] = KipMounter::all()->sortBy('name');
        $this->employees['kip_masters'] = KipMaster::all()->sortBy('name');
        return $this->employees;
    }
    public function getSourcesFromUserId()
    {
        if (Auth::check()){
            $this->user_id = Auth::user()->user_info->id;
            if(Auth::user()->user_info->user_role == 'user'){
                if(Auth::user()->user_info->position == 'мастер'){
                    $this->data_sources = Source::all()->load(['water_meters' => function ($query) {
                        $query->with(['water_meter_values' => function ($query){
                            $query->orderBy('month', 'desc')
                                ->orderBy('created_at', 'desc')->get();
                        }]);
                    }])->where('master_id', 'like', $this->user_id);
                    return $this->data_sources;
                } elseif (Auth::user()->user_info->position == 'старший мастер'){
                    $this->data_sources = Source::all()->load(['water_meters' => function ($query) {
                        $query->with(['water_meter_values' => function ($query){
                            $query->orderBy('month', 'desc')
                                ->orderBy('created_at', 'desc')->get();
                        }]);
                    }])->where('st_master_id', 'like', $this->user_id);
                    return $this->data_sources;
                } else {
                    return NULL;
                }
            }
            if(Auth::user()->user_info->user_role == 'admin'){
                $this->data_sources = Source::all()->load(['water_meters' => function ($query) {
                    $query->with(['water_meter_values' => function ($query){
                        $query->orderBy('month', 'desc')
                            ->orderBy('created_at', 'desc')->get();
                    }]);
                }]);
                return $this->data_sources;
            }
        } else {
            return NULL;
        }
    }

    public function water_meters()
    {
        return $this->hasMany(WaterMeter::class);
    }

    public function boilers()
    {
        return $this->hasMany(Boiler::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pumps()
    {
        return $this->hasMany(Pump::class);
    }

    public function master()
    {
        return $this->belongsTo(User::class, 'master_id', 'id');
    }
    public function st_master()
    {
        return $this->belongsTo(User::class, 'st_master_id', 'id');
    }

    public function el_mounter()
    {
        return $this->belongsTo(ElMounter::class);
    }
    public function el_master()
    {
        return $this->belongsTo(ElMaster::class);
    }
    public function kip_mounter()
    {
        return $this->belongsTo(KipMounter::class);
    }
    public function kip_master()
    {
        return $this->belongsTo(KipMaster::class);
    }
    public function heating_pipelines()
    {
        return $this->hasMany(HeatingPipeline::class);
    }

}
