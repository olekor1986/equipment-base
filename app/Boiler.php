<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boiler extends Model
{
    protected $fillable = ['title', 'number', 'energy_carrier', 'power', 'efficient', 'mount_year',
        'launch_year', 'serial_number', 'reg_number', 'flame_type', 'source_id', 'user_id'];

    public function source()
    {
        return $this->belongsTo(Source::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
