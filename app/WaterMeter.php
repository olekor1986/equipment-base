<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaterMeter extends Model
{
    protected $fillable = ['title', 'diameter', 'number', 'purpose', 'check_date',
        'source_id', 'note', 'condition'];
    public function scopeOfCondition($query, $condition)
    {
        return $query->where('condition','like' ,$condition)->orderBy('source_id');
    }

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function water_meter_values()
    {
        return $this->hasMany(WaterMeterValue::class);
    }

    public function water_meter_reports()
    {
        return $this->hasMany(WaterMeterReport::class);
    }
}

