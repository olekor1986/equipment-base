<?php

namespace App;

use Carbon\Carbon;
use http\Message;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WaterMeterValue extends Model
{
    protected $fillable = ['water_meter_id', 'month', 'value', 'user_id', 'after_check', 'note'];


    public function water_meter()
    {
        return $this->belongsTo(WaterMeter::class);
    }
}
