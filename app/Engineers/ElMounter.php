<?php

namespace App\Engineers;

use App\Source;
use Illuminate\Database\Eloquent\Model;

class ElMounter extends Model
{
    protected $fillable = ['name', 'w_phone', 'm_phone', 'tab_number', 'el_master_id', 'user_id'];

    public function sources()
    {
        return $this->hasMany(Source::class);
    }
}
