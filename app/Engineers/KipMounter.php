<?php

namespace App\Engineers;

use App\Source;
use Illuminate\Database\Eloquent\Model;

class KipMounter extends Model
{
    protected $fillable = ['name', 'w_phone', 'm_phone', 'tab_number', 'kip_master_id', 'user_id'];

    public function sources()
    {
        return $this->hasMany(Source::class);
    }
}
