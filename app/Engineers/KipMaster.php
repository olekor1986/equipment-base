<?php

namespace App\Engineers;

use App\Source;
use Illuminate\Database\Eloquent\Model;

class KipMaster extends Model
{
    protected $fillable = ['name', 'w_phone', 'm_phone', 'tab_number', 'user_id'];

    public function sources()
    {
        return $this->hasMany(Source::class);
    }
}
