<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pump extends Model
{
    protected $fillable = ['type', 'title', 'capacity', 'pressure', 'engine_power',
                'engine_speed', 'engine_type', 'source_id', 'user_id'];

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
