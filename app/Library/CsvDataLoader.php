<?php


namespace App\Library;

use Exception;

class CsvDataLoader
{
    /**
     * Parsing a CSV file with a specified separator.
     * @param string $filename
     * @return array
     * @throws Exception
     */
    static public function csv_to_array(string $filename):array
    {
        if (!file_exists($filename)){
            throw new Exception("File don't exists");
        }
        if (!is_readable($filename)){
            throw new Exception("Unreadable file");
        }
        $data = [];
        if (($file = fopen($filename, 'r')) !== FALSE){
            $keys = fgetcsv($file, ',');
            while(($row = fgetcsv($file, ',')) !== FALSE){
                $row = array_combine($keys, $row);
                $data[] = $row;
            }
        }
        return $data;
    }
}
