<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function boiler()
    {
        return $this->hasMany(Boiler::class);
    }

    public function source()
    {
        return $this->hasMany(Source::class);
    }

    public function source_master()
    {
        return $this->hasMany(Source::class, 'id', 'master_id');
    }

    public function source_st_master()
    {
        return $this->hasMany(Source::class, 'id', 'st_master_id');
    }

    public function pumps()
    {
        return $this->hasMany(Pump::class);
    }

    public function water_meters()
    {
        return $this->hasMany(WaterMeter::class);
    }

    public function user_info()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function heating_pipelines()
    {
        return $this->hasMany(HeatingPipeline::class);
    }
}
