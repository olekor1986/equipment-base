<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table = 'user_info';

    protected $fillable = ['first_name', 'middle_name', 'last_name', 'position',
        'w_phone', 'm_phone', 'user_role', 'user_level', 'banned', 'user_id'];


    public function user()
    {
        return $this->hasOne(User::class);
    }
}

