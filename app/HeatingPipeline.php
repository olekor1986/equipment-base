<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeatingPipeline extends Model
{
    protected $fillable = ['source_id', 'pipe_start', 'pipe_end', 'direct_diam', 'reverse_diam', 'length',
        'type', 'method', 'ins_type', 'ins_cond', 'ins_thick', 'depth', 'height', 'build_year', 'note', 'user_id'];

    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
