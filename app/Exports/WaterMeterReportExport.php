<?php

namespace App\Exports;

use App\WaterMeterReport;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;


class WaterMeterReportExport implements FromCollection, WithHeadings, WithEvents,WithColumnFormatting
{
    protected $rowsCount;

       public function setRowsCount($rowsCount): void
    {
        $this->rowsCount = $rowsCount;
    }

    public function collection()
    {
        $data =  WaterMeterReport::all('id', 'address', 'title', 'check_date',
            'previous_value', 'present_value', 'difference', 'note')
                ->sortBy('address');
        $this->setRowsCount(count($data));
        return $data;
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT
        ];
    }

    public function headings(): array
    {
        $month = Carbon::parse($_POST['month'])->locale('ru')->isoFormat('MMMM YYYY');
        $title = 'Отчет о водопотреблении ЭРР-5 за ' . $month;
        return [
            [$title],
            ['№', 'Место установки водомера', 'Марка и № водомера', 'Дата следующей поверки',
                'Предыдущие показания', 'Настоящие показания', 'V водопотребления', 'Примечание'],
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $rowsCount = $this->rowsCount + 2;
                $titleRange = 'A1:H1';
                $headerRange = 'A2:H2';
                $bodyRange = 'A3:H' . $rowsCount;
                $titleStyle = [
                    'font' => [
                        'name' => 'Arial Narrow',
                        'size' => 16,
                        'bold' => true,
                        'italic' => false,
                        'strikethrough' => false,
                        'color' => [
                            'rgb' => '000'
                        ]
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ]
                ];
                $headerStyle = [
                    'font' => [
                        'name' => 'Arial Narrow',
                        'size' => 12,
                        'bold' => true,
                        'italic' => false,
                        'strikethrough' => false,
                            'color' => [
                                'rgb' => '000'
                            ]
                         ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_HAIR,
                            'color' => ['rgb' => '000'],
                        ]
                    ],
                    'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                            'vertical' => Alignment::VERTICAL_CENTER,
                            'wrapText' => true,
                    ]
                ];
                $bodyStyle = [
                    'font' => [
                        'name' => 'Arial Narrow',
                        'size' => 12,
                        'bold' => false,
                        'italic' => false,
                        'strikethrough' => false,
                        'color' => [
                            'rgb' => '000'
                        ]
                    ],
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_HAIR,
                            'color' => ['rgb' => '000'],
                        ]
                    ],
                    'alignment' => [
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ]
                ];
                $footerStyle = [
                    'font' => [
                        'name' => 'Arial Narrow',
                        'size' => 12,
                        'bold' => false,
                        'italic' => false,
                        'strikethrough' => false,
                        'color' => [
                            'rgb' => '000'
                        ]
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                        'vertical' => Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ]
                ];
                $event->sheet->getDelegate()->getStyle($headerRange)->applyFromArray($headerStyle);
                $event->sheet->getDelegate()->getStyle($bodyRange)->applyFromArray($bodyStyle);
                $event->sheet->getDelegate()->getStyle('D3:H' . $rowsCount)->applyFromArray(
                    ['alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ]]
                );
                $event->sheet->getDelegate()->mergeCells('A1:H1');
                $event->sheet->getDelegate()->getStyle($titleRange)->applyFromArray($titleStyle);
                $widthArray = [
                    'A' => 4,
                    'B' => 20,
                    'C' => 23,
                    'D' => 9,
                    'E' => 10,
                    'F' => 10,
                    'G' => 9,
                    'H' => 12
                ];
                foreach ($widthArray as $column => $width){
                    $event->sheet->getDelegate()->getColumnDimension($column)->setWidth($width);
                }
                $event->sheet->getDelegate()->getRowDimension(1)->setRowHeight(25);
                $event->sheet->getDelegate()->getRowDimension(2)->setRowHeight(55);
                $event->sheet->getDelegate()
                    ->setCellValue('A' . ($rowsCount + 3),
                    'Начальник ЭРР-5                                        О.В. Костев');
                for ($i = 1; $i <= ($rowsCount - 2); $i++){

                    $event->sheet->getDelegate()
                        ->setCellValue('A' . ($i + 2), $i);
                }
                $event->sheet->getDelegate()->mergeCells('A' . ($rowsCount + 3) . ':H' . ($rowsCount + 3));
                $event->sheet->getDelegate()->getStyle('A' . ($rowsCount + 3))
                    ->applyFromArray($footerStyle);
            }
        ];
    }
}
