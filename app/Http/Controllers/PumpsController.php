<?php

namespace App\Http\Controllers;

use App\Http\Requests\PumpCreateRequest;
use App\Http\Requests\PumpUpdateRequest;
use App\Pump;
use App\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PumpsController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pumps = Pump::all();
        return view('pumps.index', compact('pumps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $sources = Source::all()->sortBy('address');
        return view('pumps.create', compact('sources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PumpCreateRequest $request)
    {
        $pumpsArray = $request->all();
        $pumpsArray['user_id'] = Auth::user()->id;
        Pump::create($pumpsArray);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pump  $pump
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Pump $pump)
    {
        return view('pumps.show', compact('pump'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pump  $pump
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pump $pump)
    {
        $pump['sources'] = Source::all()->sortBy('address');
        return view('pumps.edit', compact('pump'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pump  $pump
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PumpUpdateRequest $request, Pump $pump)
    {
        $pumpsArray = $request->all();
        $pumpsArray['user_id'] = Auth::user()->id;
        $pump->update($pumpsArray);
        return redirect('/pumps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pump  $pump
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Pump $pump)
    {
        $pump->delete();
        return redirect('/pumps');
    }
}
