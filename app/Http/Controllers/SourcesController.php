<?php

namespace App\Http\Controllers;

use App\Engineers\ElMaster;
use App\Engineers\ElMounter;
use App\Engineers\KipMaster;
use App\Engineers\KipMounter;
use App\Engineers\Master;
use App\Engineers\StMaster;
use App\Http\Requests\SourcesCreateRequest;
use App\Http\Requests\SourcesUpdateRequest;
use App\Source;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SourcesController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Source $source
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $sources = [];
        $sources['in_work'] = Source::all()
            ->where('in_work', 'like', '1')
            ->sortBy('address');
        $sources['not_work'] = Source::all()
            ->where('in_work', 'like', '0')
            ->sortBy('address');
        $sources['buildings'] = Source::all()
            ->where('type', 'like', 'building')
            ->sortBy('address');
        return view('source.index', compact('sources'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $employees = (new \App\Source)->getSourceEmployees();
        return view('source.create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(SourcesCreateRequest $request)
    {
        $sourceArray = $request->all();
        $sourceArray['user_id'] = Auth::user()->id;
        $sourceArray['slug'] = Str::slug($request['address']);
        Source::create($sourceArray);
        return redirect('/sources');
    }

    /**
     * Display the specified resource.
     *
     */
    public function show(Source $source)
    {
        if (!isset($source->master->user_info)){
            return redirect('/sources');
        }
        return view('source.show', compact('source'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Source  $source
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Source $source)
    {
        $source['employees'] = (new \App\Source)->getSourceEmployees();
        return view('source.edit', compact('source'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Source  $source
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(SourcesUpdateRequest $request, Source $source)
    {
        $sourceArray = $request->all();
        $sourceArray['user_id'] = Auth::user()->id;
        $sourceArray['slug'] = Str::slug($request['address']);
        $source->update($sourceArray);
        return redirect('/sources');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Source  $source
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Source $source)
    {
        $source->delete();
        return redirect('/sources');
    }
}
