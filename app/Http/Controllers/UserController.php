<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        return view('user.index', compact('user'));
    }

    public function edit(User $user)
    {
        if (Auth::user()->id != $user->id){
            return redirect('/');
        }
        return view('user.edit', compact('user'));
    }


}
