<?php

namespace App\Http\Controllers;

use App\InteractiveMap;
use App\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InteractiveMapController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if (Auth::user()->user_info->user_role != 'user' ||
            Auth::user()->user_info->user_role != 'admin' ){
            $sources = (new Source)->getSourcesFromUserId()
                ->where('in_work', 'like', true);
            if($sources == NULL){
                return redirect('/');
            }
            $count = count($sources);
            foreach ($sources as $key => $source){
                $coordinates = explode(',', $source->gps);
                $sources[$key]['coordinate_0'] = trim($coordinates[0]);
                $sources[$key]['coordinate_1'] = trim($coordinates[1]);
            }
            return view('interactive_map.index', compact('sources', 'count'));
        } else {
            return redirect('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InteractiveMap  $interactiveMap
     * @return \Illuminate\Http\Response
     */
    public function show(InteractiveMap $interactiveMap)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InteractiveMap  $interactiveMap
     * @return \Illuminate\Http\Response
     */
    public function edit(InteractiveMap $interactiveMap)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InteractiveMap  $interactiveMap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InteractiveMap $interactiveMap)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InteractiveMap  $interactiveMap
     * @return \Illuminate\Http\Response
     */
    public function destroy(InteractiveMap $interactiveMap)
    {
        //
    }
}
