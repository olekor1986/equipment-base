<?php

namespace App\Http\Controllers;

use App\HeatingPipeline;
use App\Source;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class HeatingPipelineController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $sources = Source::all()
            ->where('in_work', 'like', 1)
            ->sortBy('address');
        return view('heating_pipelines.index', compact('sources'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $sources = Source::all()
            ->where('in_work', 'like', 1)
            ->sortBy('address');
        return view('heating_pipelines.create', compact('sources'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $new_pipe_data = $request->all();
        $new_pipe_data['length'] = floatval(str_replace(',', '.', $new_pipe_data['length']));
        $new_pipe_data['height'] = floatval(str_replace(',', '.', $new_pipe_data['height']));
        $new_pipe_data['ins_thick'] = intval($new_pipe_data['ins_thick']);
        $new_pipe_data['user_id'] = Auth::user()->id;
        HeatingPipeline::create($new_pipe_data);
        return redirect('/heating_pipelines');
    }

    /**
     * Display the specified resource.
     */
    public function show(HeatingPipeline $heatingPipeline)
    {
        return view('heating_pipelines.show', compact('heatingPipeline'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(HeatingPipeline $heatingPipeline)
    {
        $sources = Source::all()
            ->where('in_work', 'like', 1)
            ->sortBy('address');
        return view('heating_pipelines.edit', compact('heatingPipeline', 'sources'));
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request, HeatingPipeline $heatingPipeline)
    {
        $update_pipe_data = $request->all();
        $update_pipe_data['length'] = floatval(str_replace(',', '.', $update_pipe_data['length']));
        $update_pipe_data['height'] = floatval(str_replace(',', '.', $update_pipe_data['height']));
        $update_pipe_data['ins_thick'] = intval($update_pipe_data['ins_thick']);
        $update_pipe_data['user_id'] = Auth::user()->id;
        $heatingPipeline->update($update_pipe_data);
        return redirect('/heating_pipelines');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(HeatingPipeline $heatingPipeline)
    {
        $heatingPipeline->delete();
        return redirect('/heating_pipelines');
    }
}
