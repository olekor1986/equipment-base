<?php

namespace App\Http\Controllers;

use App\HeatingPipelineRepair;
use Illuminate\Http\Request;

class HeatingPipelineRepairController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HeatingPipelineRepair  $heatingPipelineRepair
     * @return \Illuminate\Http\Response
     */
    public function show(HeatingPipelineRepair $heatingPipelineRepair)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HeatingPipelineRepair  $heatingPipelineRepair
     * @return \Illuminate\Http\Response
     */
    public function edit(HeatingPipelineRepair $heatingPipelineRepair)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HeatingPipelineRepair  $heatingPipelineRepair
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HeatingPipelineRepair $heatingPipelineRepair)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HeatingPipelineRepair  $heatingPipelineRepair
     * @return \Illuminate\Http\Response
     */
    public function destroy(HeatingPipelineRepair $heatingPipelineRepair)
    {
        //
    }
}
