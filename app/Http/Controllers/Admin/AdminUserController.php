<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminUserCreateRequest;
use App\UserInfo;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminUserController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $users = User::all()->load('user_info');
        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(AdminUserCreateRequest $request)
    {
        $new_user_data = $request->all();
        $new_user_data['password'] = Hash::make($new_user_data['password']);
        User::create($new_user_data);
        return redirect('/admin/users');
    }

    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }


    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));
    }


    public function update(Request $request, User $user)
    {
        $userData = $request->all();
        $user->update($userData);
        return back();
    }


    public function destroy(User $user)
    {
        if (isset($user->user_info->id)){
            $user_info_id = $user->user_info->id;
            $user_info = UserInfo::find($user_info_id);
            $user_info->delete();
        }
        $user->delete();
        return redirect('/admin/users');
    }
}
