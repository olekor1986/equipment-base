<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
       return view('admin.index');
    }
    public function engineers()
    {
        return view('admin.engineers');
    }

}
