<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\UserInfo;
use Illuminate\Http\Request;

class AdminUserInfoController extends Controller
{
    public function  __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return redirect('/admin/users');
    }

     public function create($user_id)
    {
        return view('admin.user_info.create', compact('user_id'));
    }

    public function store(Request $request)
    {
        $new_user_info_data = $request->all();
        UserInfo::create($new_user_info_data);
        return redirect('/admin/users');
    }

    public function update(Request $request, UserInfo $user_info)
    {
        $user_info_data = $request->all();
        $user_info->update($user_info_data);
        return back();
    }

    public function destroy($id)
    {
        //
    }
}
