<?php

namespace App\Http\Controllers;

use App\WaterMeterReport;
use App\Exports\WaterMeterReportExport;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class WaterMeterReportController extends Controller
{
    protected $currentMonth;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $water_meter_reports = (new WaterMeterReport)->getAllMonthsFromValuesTable();
        return view('water_meters.report.index', compact('water_meter_reports'));
    }

    public function report()
    {
        $report = (new WaterMeterReport)->getReport();
        return view('water_meters.report.output', compact('report'));
    }

    public function export()
    {
        (new WaterMeterReport)->getReport();
        $month = Carbon::parse($_POST['month'])->locale('ru')->isoFormat('MMMM YYYY');
        $filename = 'Отчет о водопотреблении ЭРР-5 за ' . $month . '.xls';
        return Excel::download(new WaterMeterReportExport, $filename);
    }


}
