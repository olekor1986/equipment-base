<?php

namespace App\Http\Controllers;

use App\Http\Requests\WaterMeterValuesCreateRequest;
use App\Http\Requests\WaterMeterValuesUpdateRequest;
use App\Source;
use App\WaterMeter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\WaterMeterValue;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class WaterMeterValuesController extends Controller
{
    protected $dateNow;
    protected $datePrev;
    protected $monthNow;

    public function __construct()
    {
        $this->middleware('auth');
        $this->dateNow = Carbon::now()->locale('ru');
        $this->datePrev = Carbon::now()->subMonth()->locale('ru');
        $this->monthNow = $this->dateNow->isoFormat('YYYY-MM');

    }

    public function create()
    {
        $sources = (new Source)->getSourcesFromUserId();
        if($sources == NULL){
            return redirect('/');
        }
        $dateNow = $this->dateNow;
        $datePrev = $this->datePrev;
        return view('water_meter_values.create', compact('sources', 'dateNow', 'datePrev'));
    }

    public function store(WaterMeterValuesCreateRequest $request)
    {
        $newValues = $request->all();
        if(isset($newValues['after_check'])){
            if($newValues['after_check'] == 'on'){
                $newValues['after_check'] = true;
            }
        }
        $newValues['user_id'] = Auth::user()->id;
        WaterMeterValue::create($newValues);
        return back();
    }

    public function show(WaterMeterValue $waterMeterValue)
    {
        //
    }

    public function edit(WaterMeterValue $water_meter_value)
    {
        return view('water_meter_values.edit', compact('water_meter_value'));
    }


    public function update(Request $request, WaterMeterValue $waterMeterValue)
    {
        $updateValues = $request->all();
        if(isset($updateValues['after_check'])){
            if($updateValues['after_check'] == 'on'){
                $updateValues['after_check'] = true;
            }
        } else {
            $updateValues['after_check'] = false;
        }
        $updateValues['user_id'] = Auth::user()->id;
        $path = '/water_meters/' . $updateValues['water_meter_id'];
        $waterMeterValue->update($updateValues);
        return redirect($path);
    }


    public function destroy(WaterMeterValue $waterMeterValue)
    {
        $waterMeterValue->delete();
        return back();
    }
}
