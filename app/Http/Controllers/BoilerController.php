<?php

namespace App\Http\Controllers;

use App\Boiler;
use App\Http\Requests\BoilersCreateRequest;
use App\Http\Requests\BoilersUpdateRequest;
use App\Source;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoilerController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $boilers = Boiler::all();
        return view('boilers.index', compact('boilers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Source $source
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $sources = Source::all()->sortBy('address')->sortBy('number');
        return view('boilers.create', compact('sources'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BoilersCreateRequest $request)
    {
        $boilersArrray = $request->all();
        $boilersArrray['user_id'] = Auth::user()->id;
        Boiler::create($boilersArrray);
        return redirect('/boilers');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Boiler  $boiler
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Boiler $boiler)
    {
        return view('boilers.show', compact('boiler'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Boiler  $boiler
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Boiler $boiler)
    {
        return view('boilers.edit', compact('boiler'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Boiler  $boiler
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(BoilersUpdateRequest $request, Boiler $boiler)
    {
        $boilerArray = $request->all();
        $boilerArray['user_id'] = Auth::user()->id;
        $boiler->update($boilerArray);
        return redirect('/boilers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Boiler  $boiler
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Boiler $boiler)
    {
        $boiler->delete();
        return redirect('/boilers');
    }
}
