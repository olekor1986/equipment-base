<?php

namespace App\Http\Controllers;

use App\Http\Requests\WaterMeterCreateRequest;
use App\Http\Requests\WaterMeterUpdateRequest;
use App\WaterMeter;
use App\WaterMeterValue;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Source;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class WaterMeterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $water_meters['in_work'] = WaterMeter::ofCondition('в работе')->get();
        $water_meters['defective'] = WaterMeter::ofCondition('брак')->get();
        $water_meters['reserve'] = WaterMeter::ofCondition('резерв')->get();
        return view('water_meters.index', compact('water_meters'));
    }


    public function create()
    {
        $sources = Source::all()->sortBy('address');
        return view('water_meters.create', compact('sources'));
    }


    public function store(WaterMeterCreateRequest $request)
    {
        $waterMeterNewData = $request->all();
        $newValues['user_id'] = $waterMeterNewData['user_id'] = Auth::user()->id;
        $water_meter = WaterMeter::create($waterMeterNewData);

        $datePrev = Carbon::now()->subMonth()->locale('ru');
        $monthPrev = $datePrev->isoFormat('YYYY-MM');
        $newValues['after_check'] = true;
        $newValues['month'] = $monthPrev;
        $newValues['water_meter_id'] = $water_meter->id;
        if (empty($waterMeterNewData['primary_value'])){
            $newValues['value'] = '00000';
        } else {
            $newValues['value'] = $waterMeterNewData['primary_value'];
        }
        WaterMeterValue::create($newValues);

        return redirect('/water_meters');
    }

    public function show(WaterMeter $water_meter)
    {
        $water_meter['sources'] = Source::all()->sortBy('address');
        $water_meter->load(['water_meter_values' => function($query){
            $query->orderBy('month', 'desc')
                ->orderBy('created_at', 'desc')
                ->limit(6);
        }]);

        if (Auth::user()->user_info->user_role == 'user'){
            if (Auth::user()->user_info->position == 'мастер' && $water_meter->source->master_id == Auth::user()->id){
                return view('water_meters.show', compact('water_meter'));
            } elseif (Auth::user()->user_info->position == 'старший мастер' && $water_meter->source->st_master_id == Auth::user()->id) {
                return view('water_meters.show', compact('water_meter'));
            } else {
                return redirect('/water_meters');
            }
        } elseif (Auth::user()->user_info->user_role == 'admin' || Auth::user()->user_info->user_role == 'guest') {
            return view('water_meters.show', compact('water_meter'));
        } else {
            return redirect('/water_meters');
        }
    }

    public function update(WaterMeterUpdateRequest $request, WaterMeter $waterMeter)
    {
        $waterMeterUpdateData = $request->all();
        $waterMeterUpdateData['user_id'] = Auth::user()->id;
        $waterMeter->update($waterMeterUpdateData);
        return back();
    }

    public function destroy(WaterMeter $waterMeter)
    {
        $waterMeter->delete();
        return redirect('/water_meters');
    }

}
