<?php

namespace App\Http\Controllers;

use App\Source;

class MonitoringController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $sources = Source::all()
            ->where('monitoring', 'like', '1')
            ->sortBy('address');
        return view('monitoring.index', compact('sources'));
    }
}
