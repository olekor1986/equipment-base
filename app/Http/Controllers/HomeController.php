<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function  __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        if (Auth::check()) {
            if (isset(Auth::user()->user_info->user_role)) {
                if (Auth::user()->user_info->user_role == 'admin') {
                    return redirect('/admin');
                }
            } else {
                return view('user_info.create');
            }
        }
        return view('homepage.index');
    }
}
