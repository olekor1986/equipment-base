<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WaterMeterCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2',
            'source_id' => 'required',
            'diameter' => 'required',
            'purpose' => 'required',
            'number' => 'required|min:4',
            'check_date' => 'required'
        ];
    }
}
