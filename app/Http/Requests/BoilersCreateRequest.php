<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BoilersCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:4|max:30',
            'source_id' => 'required',
            'number' => 'required',
            'energy_carrier' => 'required',
            'power' => 'required',
            'efficient' => 'required|min:2|max:2',
            'flame_type' => 'required'
        ];
    }
}
