<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SourcesCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address' => 'required|min:5|max:50',
            'type' => 'required',
            'fuel' => 'required',
            'full_power'  => 'required',
            'connected_power'  => 'required',
            'district'  => 'required',
            'master_id'  => 'required',
            'st_master_id'  => 'required',
            'el_mounter_id'  => 'required',
            'el_master_id'  => 'required',
            'kip_mounter_id'  => 'required',
            'kip_master_id'  => 'required'
        ];
    }
}
